package com.ebiz.msoclistar.impl;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
//import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
//import java.util.Collections;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.ebiz.msoclistar.Oc;
import com.ebiz.msoclistar.OcListarRepository;
import java.util.UUID;

@Repository
public class OcListarRepositoryImpl implements OcListarRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;
    private char perfil;
    private int origen;
    private List<String> lista_columnas;
    private String date_format = "dd/MM/yyyy";								// por default
    private SimpleDateFormat sdf = new SimpleDateFormat(date_format);
    
    private final Logger logger = LoggerFactory.getLogger(OcListarRepositoryImpl.class);

	public List<Oc> list(String ruc_proveedor, String razonsocial_proveedor, String ruc_comprador,
            String razonsocial_comprador, String nro_rfq, String nro_oc, String estado, Date fechainicio,
            Date fechafin, List<String> tipo_oc, String org_id, String order_columns, String org_idcomprador) {
		//String SQL = "SELECT * FROM oc.vm_listaroc ";
        StringBuilder SQL_build = new StringBuilder("SELECT * FROM oc.vm_listaroc ");
		List<String> condiciones = new ArrayList<>();
        List<Object> parametros = new ArrayList<>();
        boolean nro_seguimiento_agregado = false;
        
        if (perfil=='c') {  // CASO : COMPRADOR
        	
        	if (origen==0) {		// Sólo posible para el PEESEACE
	        	if (StringUtils.isNotBlank(ruc_proveedor)) {
	                condiciones.add("upper(vc_rucproveedor) like ? ");
	                parametros.add("%" + ruc_proveedor.toUpperCase() + "%");
	            }
	        	if (StringUtils.isNotBlank(razonsocial_proveedor)) {
	                condiciones.add("upper(vc_razonsocialproveedor) like ? ");
	                parametros.add("%" + razonsocial_proveedor.toUpperCase() + "%");
	            }
        	}
        	
        	// Posible para PEESEACE y PEB2M
        	if (StringUtils.isNotBlank(estado)) {								// debe recibir la descripción corta del estado
            	if (!estado.equalsIgnoreCase("NONE")) {
            		condiciones.add("vc_estadocomprador = ? ");
                    parametros.add(estado);
				}
				if (StringUtils.isNotBlank(org_id)) {
            		condiciones.add("in_idorganizacioncompradora = ? ");
                    parametros.add(UUID.fromString(org_id));
            	}
            }
        }
        else {				// CASO : 	PROVEEDOR
        	
        	if (origen==0) {		// Sólo posible para el PEESEACE
	        	if (StringUtils.isNotBlank(ruc_comprador)) {
	                condiciones.add("upper(vc_ruccomprador) like ? ");
	                parametros.add("%" + ruc_comprador.toUpperCase() + "%");
	            }
	            if (StringUtils.isNotBlank(razonsocial_comprador)) {
	                condiciones.add("upper(vc_razonsocialcomprador) like ? ");
	                parametros.add("%" + razonsocial_comprador.toUpperCase() + "%");
	            }
        	}
            
        	// Posible para PEESEACE y PEB2M
            if (StringUtils.isNotBlank(estado)) {									// debe recibir la descripción corta del estado
            	if (!estado.equalsIgnoreCase("NONE")) {
            		condiciones.add("vc_estadoproveedor = ? ");
                    parametros.add(estado);
				}
				/*if (StringUtils.isNotBlank(org_id)) {
            		condiciones.add("in_idorganizacionproveedora = ? ");
                    parametros.add(UUID.fromString(org_id));
            	}*/
            }
            
            if (StringUtils.isNotBlank(org_id)) {
        		condiciones.add("in_idorganizacionproveedora = ? ");
                parametros.add(UUID.fromString(org_id));
        	}
            
            if (StringUtils.isNotBlank(org_idcomprador)) {
                condiciones.add("in_idorganizacioncompradora = ? ");
                parametros.add(UUID.fromString(org_idcomprador));
            }
        }
        // PARA AMBOS CASOS
        if (tipo_oc != null && !tipo_oc.isEmpty()) {
            String posibles = "('" + tipo_oc.get(0).toUpperCase() + "'";
            for (int i=1;i<tipo_oc.size();i++) {
            	posibles+=",'";
            	posibles+=tipo_oc.get(i).toUpperCase()+"'";
            }
            posibles+=") ";
            condiciones.add("upper(vc_tipooc) in " + posibles);
        }
        /*
        if (estado != null) {
        	if (!estado.equalsIgnoreCase("NONE")) {
        		condiciones.add("ch_estado = ? ");
                parametros.add(estado);
        	}
        }
        */
        
	    if (origen==0) {				// Sólo posible para PEESEACE    
	        if (StringUtils.isNotBlank(nro_rfq)) {
	            condiciones.add("upper(ch_numerorfq) like ? ");
	            parametros.add("%" + nro_rfq.toUpperCase() + "%");
	            nro_seguimiento_agregado = true;
	        }
	    }
	    
        if (StringUtils.isNotBlank(nro_oc)) {
            condiciones.add("upper(ch_numeroseguimiento) like ? ");
            parametros.add("%" + nro_oc.toUpperCase() + "%");
            nro_seguimiento_agregado = true;
        }
        
        //nro_seguimiento_agregado = false;
        
        if (!nro_seguimiento_agregado) {
            condiciones.add("ts_fecharegistro >= ? AND ts_fecharegistro <= ? ");
            parametros.add(fechainicio);
            parametros.add(fechafin);
        }

        if (condiciones.size() > 0) {
            //SQL += "WHERE " + condiciones.get(0);
        	SQL_build.append("WHERE ").append(condiciones.get(0));
        }
        for (int i = 1; i < condiciones.size(); i++) {
            //SQL += "AND " + condiciones.get(i);
        	SQL_build.append("AND ").append(condiciones.get(i));
        }
        
        //SQL += order_columns+";";
        SQL_build.append(order_columns).append(";");
        String SQL = SQL_build.toString();
        List<Oc> ocs = null;
        
        try {
            ocs = jdbcTemplate.query(SQL, parametros.toArray(), this::mapParam);
            parametros.clear();
            condiciones.clear();
        } catch (EmptyResultDataAccessException e) {
            e.printStackTrace();
            logger.info(e.getMessage().toString());
        }
        logger.info("Hay " + ocs.size()+" Órdenes");
        return ocs;
    }

    public Oc mapParam(ResultSet rs, int i) throws SQLException {
        Oc oc = new Oc();
        for (int j=0;j<lista_columnas.size();j++) {
        	String columna = lista_columnas.get(j);
        	switch(columna) {
	        	case "in_idoc":
	        		oc.setIn_idoc(StringUtils.trimToEmpty(rs.getString("in_idoc")));
	        		break;
	        	case "ch_numeroseguimiento":
	        		oc.setVc_numeroseguimiento_oc(StringUtils.trimToEmpty(rs.getString("ch_numeroseguimiento")));
	        		break;
	        	case "vc_estado":
	        		if (perfil=='c')
	        			oc.setVc_estado(StringUtils.trimToEmpty(rs.getString("vc_estado_comprador")));
	        		else if (perfil=='p')
	        			oc.setVc_estado(StringUtils.trimToEmpty(rs.getString("vc_estado_proveedor")));
	        		break;
	        	case "vc_tipooc":
	        		if (StringUtils.trimToEmpty(rs.getString("vc_tipooc")).equals("M"))
	        			oc.setVc_tipooc("Material");
	        		else if (StringUtils.trimToEmpty(rs.getString("vc_tipooc")).equals("S"))
	        			oc.setVc_tipooc("Servicio");
	        		break;
	        	case "vc_atenciona":
	        		oc.setVc_atenciona(StringUtils.trimToEmpty(rs.getString("vc_atenciona")));
	        		break;
	        	case "vc_ruccomprador":
	        		if (perfil=='p')
	        			oc.setVc_ruccomprador(StringUtils.trimToEmpty(rs.getString("vc_ruccomprador")));
	        		break;
	        	case "vc_nombreusuariocreadopor":
	        		//if (perfil=='p')
	        			oc.setVc_nombre_usrcomprador(StringUtils.trimToEmpty(rs.getString("vc_nombreusuariocreadopor")));
	        		break;
	        	case "vc_razonsocialcomprador":
	        		if (perfil=='p')
	        		oc.setVc_razonsocialcomprador(StringUtils.trimToEmpty(rs.getString("vc_razonsocialcomprador")));
	        		break;
	        	case "vc_rucproveedor":
	        		if (perfil=='c')
	        			oc.setVc_rucproveedor(StringUtils.trimToEmpty(rs.getString("vc_rucproveedor")));
	        	case "vc_nombreusuarioproveedor":
	        		if (perfil=='c')
	        			oc.setVc_nombre_usrproveedor(StringUtils.trimToEmpty(rs.getString("vc_nombreusuarioproveedor")));
	        		break;
	        	case "vc_razonsocialproveedor":
	        		if (perfil=='c')
	        			oc.setVc_razonsocialproveedor(StringUtils.trimToEmpty(rs.getString("vc_razonsocialproveedor")));
	        		break;
	        	case "fl_valortotal":
	        		oc.setFl_valortotal(rs.getDouble("fl_valortotal"));
	        		break;
	        	case "vc_moneda":
	        		oc.setVc_moneda(StringUtils.trimToEmpty(rs.getString("vc_moneda")));
	        		break;
	        	case "ts_fechacreacion":
	        		//oc.setTs_fechacreacion(StringUtils.trimToEmpty(rs.getString("ts_fechacreacion")));
	        		//oc.setTs_fechacreacion(rs.getDate("ts_fechacreacion"));
	        		oc.setTs_fechacreacion(sdf.format(rs.getDate("ts_fechacreacion")));
	        		break;
	        	case "ch_numerorfq":
	        		oc.setVc_numeroseguimiento_rfq(StringUtils.trimToEmpty(rs.getString("ch_numerorfq")));
	        		break;
	        	case "in_version":
	        		if (origen==1)			// Sólo posible para PEB2M
	        			oc.setIn_version(rs.getInt("in_version"));
	        		break;
	        	case "ts_fecharegistro":
	        		if (origen==1)
	        			oc.setTs_fecharegistro(sdf.format(rs.getDate("ts_fecharegistro")));
	        		break;
	        	/*
	        	case "CompradorUsuarioID":
	        		oc.setId_usuariocomprador(StringUtils.trimToEmpty(rs.getString("in_idusuariocomprador")));
	        		break;
	        	case "IdUsuarioProveedor":
	        		oc.setId_usuarioproveedor(StringUtils.trimToEmpty(rs.getString("in_idusuarioproveedor")));
	        		break;
	        	case "CompradorOrgID":
	        		oc.setId_organizacioncompradora(StringUtils.trimToEmpty(rs.getString("in_idorganizacioncompradora")));
	        		break;
	        	case "id_organizacionprov":
	        		oc.setId_organizacionproveedora(StringUtils.trimToEmpty(rs.getString("in_idorganizacionproveedora")));
	        		break;
	        	*/
	        	
        	}
        	
        	
        }
        
        //oc.setId_oc(StringUtils.trimToEmpty(rs.getString("in_idoc")));
        //oc.setNro_oc(StringUtils.trimToEmpty(rs.getString("ch_numeroseguimiento")));
        //oc.setEstado(StringUtils.trimToEmpty(rs.getString("ch_estado")));
        //oc.setTipo_oc(StringUtils.trimToEmpty(rs.getString("vc_tipooc")));
        //oc.setId_usuariocomprador(StringUtils.trimToEmpty(rs.getString("in_idusuariocomprador")));
        //oc.setNombre_usuariocomprador(StringUtils.trimToEmpty(rs.getString("vc_nombreusuariocomprador")));
        //oc.setId_usuarioproveedor(StringUtils.trimToEmpty(rs.getString("in_idusuarioproveedor")));
        //oc.setNombre_usuarioproveedor(StringUtils.trimToEmpty(rs.getString("vc_nombreusuarioproveedor")));
        //oc.setAtenciona(StringUtils.trimToEmpty(rs.getString("vc_atenciona")));
        //oc.setId_organizacioncompradora(StringUtils.trimToEmpty(rs.getString("in_idorganizacioncompradora")));
        //oc.setRazonsocial_comprador(StringUtils.trimToEmpty(rs.getString("vc_razonsocialcomprador")));
        //oc.setId_organizacionproveedora(StringUtils.trimToEmpty(rs.getString("in_idorganizacionproveedora")));
        //oc.setRazonsocial_proveedor(StringUtils.trimToEmpty(rs.getString("vc_razonsocialproveedor")));
        //oc.setValortotal(rs.getDouble("fl_valortotal"));
        //oc.setMoneda(StringUtils.trimToEmpty(rs.getString("vc_moneda")));
        //oc.setFechacreacion(StringUtils.trimToEmpty(rs.getString("ts_fechacreacion")));
        
        return oc;
    }
    
    public List<String> getLista_columnas() {
		return lista_columnas;
	}

	public void setLista_columnas(List<String> lista_columnas) {
		this.lista_columnas = lista_columnas;
	}

	public char getPerfil() {
		return perfil;
	}

	public void setPerfil(char perfil) {
		this.perfil = perfil;
	}

	public String getDate_format() {
		return date_format;
	}

	public void setDate_format(String date_format) {
		this.date_format = date_format;
	}

	public int getOrigen() {
		return origen;
	}

	public void setOrigen(int origen) {
		this.origen = origen;
	}
	
}