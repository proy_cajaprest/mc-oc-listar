package com.ebiz.msoclistar;

import java.util.List;

import com.ebiz.msoclistar.Oc;

import java.sql.Date;
//import java.sql.Timestamp;

public interface OcListarRepository {

    List<Oc> list(String ruc_proveedor, String razonsocial_proveedor, String ruc_comprador,
            String razonsocial_comprador, String nro_rfq, String nro_oc, String estado, Date fechainicio,
            Date fechafin, List<String> tipo_oc, String org_id, String order_columns, String org_idcomprador);
    
    List<String> getLista_columnas();
    
    void setLista_columnas(List<String> lista_columnas);
    
    char getPerfil();
    
    void setPerfil(char perfil);
    
    String getDate_format();
    
    void setDate_format(String date_format);
    
    int getOrigen();
    
    void setOrigen(int origen);
    
}
