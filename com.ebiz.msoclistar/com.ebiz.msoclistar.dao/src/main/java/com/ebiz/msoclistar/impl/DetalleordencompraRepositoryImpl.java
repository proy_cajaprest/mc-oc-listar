package com.ebiz.msoclistar.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.ebiz.msoclistar.Ordencompra;
import com.ebiz.msoclistar.ProductoxOC;
import com.ebiz.msoclistar.SubitemxProductoxOC;
import com.ebiz.msoclistar.ArchivoxOC;
import com.ebiz.msoclistar.AtributoxOC;
import com.ebiz.msoclistar.AtributoxProductoxOC;
import com.ebiz.msoclistar.AtributoxSubitemxProductoxOC;
import com.ebiz.msoclistar.DetalleordencompraRepository;

@Repository
public class DetalleordencompraRepositoryImpl implements DetalleordencompraRepository{

	@Autowired
	private JdbcTemplate jdbcTemplate;
	private int origen;
	private int cabecera;
	private int solo_productos;
	private char perfil;
	private String date_format = "dd/MM/yyyy";								// por default
	private SimpleDateFormat sdf = new SimpleDateFormat(date_format);

	public Ordencompra detalleOrdenCompra(String id) {
		Ordencompra ordencompra = new Ordencompra();
		String SQL = "";
		/*
		SQL = "select in_idoc,in_idusuariocomprador,in_idusuarioproveedor,vc_nombreusuariocomprador,vc_nombreusuarioproveedor,ch_numeroseguimiento,in_version,ch_prioridad,in_idqt,ts_fecharegistro,vc_lugarentrega,vc_terminosentrega,vc_terminos,vc_consignatario, ts_fechaentrega,ts_fechaenvio,vc_paisembarque,vc_regionembarque,vc_condicionembarque,vc_embarcador,in_embarqueparcial,vc_puertodesembarque,vc_polizaseguro,vc_aduana,vc_requiereinspeccion,vc_companiainspectora,ch_numeroinspeccion ,vc_autorizadopor ,vc_cargo, in_tiemporecordatorio,fl_valortotal,fl_valorventa,fl_subtotal,ts_fechaautorizacion,vc_moneda,vc_comentario,fl_preciofob,ts_fechaembarque,ch_codigoalmacenenvio,vc_lab,vc_numeroap,vc_forwarder,fl_isc,in_flagorigen,vc_campo1,vc_campo2,vc_campo3,vc_campo4,vc_campo5,vc_campo6,vc_campo7,vc_campo8,in_codigousuariocreacion,in_codigousuariomodificacion,ts_fechahoracreacion,ts_fechahoramodificacion,in_habilitado,vc_estadoproveedor,vc_estadocomprador,vc_obsestado,ts_fechacreacion,vc_tipotransporte,fl_tasacambio,vc_chequepara,in_codalmacenenvio,ch_codigoalmacenenvio, vc_narrativa,vc_cliente,vc_tipoprecio,in_otrosimpuestos,ts_fechamodoc,in_flagmodoc,vc_numqtprov,vc_msgmail,in_idoperadorlog,ch_estadob2seller,in_idorganizacioncompradora,in_idorganizacionproveedora,fl_impuestos,vc_precioqt,ch_numerorfq,vc_nombrerfq,ch_numeroqt,in_versionqt,vc_pagaraproveedor,vc_rucproveedor,in_codigoproveedorcambioestado,vc_comentarioproveedor,fl_descuentoporcentaje,fl_impuestosporcentaje,ts_fechavisualizacion,vc_monmig,vc_logo,vc_firma,in_flagocdirecta,in_requiereinspeccion,in_numeroap,fl_precioqt,vc_versioncliente,vc_apnumber,vc_coderror,vc_descerror,ts_fechadocumentooc,in_idorgpropietaria,ts_fechanotificacion,vc_tipooc,vc_enviarfacturaa,de_iva,de_utilidades,de_gastosgen,vc_forwarder";      

		if(origen == 1) { //B2M
			SQL += ",vc_ruccomprador,vc_razonsocialproveedor,vc_atenciona,vc_emailcontacto,vc_facturara,vc_condicionpago,fl_valorcondpago,vc_unidadcondpago,vc_direccionfactura";			
			// falta campo razon social comprador, ,codigoproveedor,grupocompra,fechainiciocontrato,fechafincontrato	
			
		}else if(origen == 0) {// Eseace
			SQL += ",vc_objetocontratacion,vc_tipocontratacion,vc_numeroexpedientesiaf,ts_fechacompromisoppto,fl_tipocambio,in_tipo,vc_descripcioncontratacion,vc_unidadessolicitantes,vc_unidadesinformantes,vc_comentariocomprador,fl_otroscostos,fl_descuento";
		}
		SQL += " from oc.t_oc where in_idoc = ?";
			
		*/
		
		
		SQL ="SELECT * FROM oc.vm_listaroc where in_idoc = ?";
		
		List<Ordencompra> l_ordencompra = null;
		try{
			l_ordencompra = jdbcTemplate.query(SQL,new Object[] { UUID.fromString(id) }, this::mapParam);
		}catch (EmptyResultDataAccessException e) {
			e.printStackTrace();
		}
		
		
		if (l_ordencompra != null && l_ordencompra.size()>0)
			ordencompra = l_ordencompra.get(0);
		else return null;
		
		SQL = "select * from oc.t_atributoxoc where in_idoc = ?";

		List<AtributoxOC> l_atributoxOC = null;
		try{
			l_atributoxOC = jdbcTemplate.query(SQL,new Object[] { UUID.fromString(id) }, this::mapParam4);
		}catch (EmptyResultDataAccessException e) {
			e.printStackTrace();
		}
		
	
		ordencompra.setLista_atributoxoc(l_atributoxOC);
		
		
		SQL = "select * from oc.t_archivoxoc where in_idoc = ?";

		List<ArchivoxOC> l_archivoxOC = null;
		try{
			l_archivoxOC = jdbcTemplate.query(SQL,new Object[] { UUID.fromString(id) }, this::mapParam5);
		}catch (EmptyResultDataAccessException e) {
			e.printStackTrace();
		}
		
		ordencompra.setLista_archivoxoc(l_archivoxOC);
			
		
		SQL = "select * from oc.t_productoxoc where in_idoc = ?";
        
		List<ProductoxOC> l_prodxoc = null;
		try{
			l_prodxoc = jdbcTemplate.query(SQL,new Object[] { UUID.fromString(id) }, this::mapParam2);
		}catch (EmptyResultDataAccessException e) {
			e.printStackTrace();
		}
		
		if(l_prodxoc != null && l_prodxoc.size()>0 && solo_productos==0) {
			for(ProductoxOC productoxOC : l_prodxoc) {
				
				
				SQL = "select * from oc.t_atributoxproductoxoc where in_idoc = ? and in_idproductoxoc = ?";				
				List<AtributoxProductoxOC> l_atxprodxoc = null;
				try{
					l_atxprodxoc = jdbcTemplate.query(SQL,new Object[] { UUID.fromString(id) ,UUID.fromString(productoxOC.getIn_idproductoxoc()) }, this::mapParam3);
				}catch (EmptyResultDataAccessException e) {
					e.printStackTrace();
				}
				
				SQL = "select * from oc.t_subitemxprodxoc where in_idoc = ? and in_idproductoxoc = ?";
				List<SubitemxProductoxOC> l_subitemxprodxoc = null;
				try {
					l_subitemxprodxoc = jdbcTemplate.query(SQL,new Object[] { UUID.fromString(id) ,UUID.fromString(productoxOC.getIn_idproductoxoc()) }, this::mapParam6);
				}catch(EmptyResultDataAccessException e){
					e.printStackTrace();
				}
				
				if (l_subitemxprodxoc != null && l_subitemxprodxoc.size()>0) {
					
					for(SubitemxProductoxOC subitemxProductoxOC : l_subitemxprodxoc) {
						SQL = "select * from oc.t_atributoxsubitemxprodxoc where in_idsubitemxprodxoc = ?";
						List<AtributoxSubitemxProductoxOC> l_atxsubitem = null;
						try {
							l_atxsubitem = jdbcTemplate.query(SQL,new Object[] { UUID.fromString(subitemxProductoxOC.getIn_idsubitemxprodxoc()) }, this::mapParam7);
						}catch(EmptyResultDataAccessException e){
							e.printStackTrace();
						}
						subitemxProductoxOC.setLista_atributoxsubitem(l_atxsubitem);
					}
				}
				productoxOC.setLista_atributoxprod(l_atxprodxoc);
				productoxOC.setLista_subitemxprod(l_subitemxprodxoc);
			}
		}
		ordencompra.setLista_prodxoc(l_prodxoc);
		return ordencompra;
	}
	
	
	public List<ProductoxOC> detalleProductosOC(String id) {
				
		String SQL = "select * from oc.t_productoxoc where in_idoc = ?";
        
		List<ProductoxOC> l_prodxoc = null;
		try{
			l_prodxoc = jdbcTemplate.query(SQL,new Object[] { UUID.fromString(id) }, this::mapParam2);
		}catch (EmptyResultDataAccessException e) {
			e.printStackTrace();
		}
		
		if(l_prodxoc != null && l_prodxoc.size()>0 && solo_productos==0) {
			for(ProductoxOC productoxOC : l_prodxoc) {
				SQL = "select * from oc.t_atributoxproductoxoc where in_idoc = ? and in_idproductoxoc = ?";				
				List<AtributoxProductoxOC> l_atxprodxoc = null;
				try{
					l_atxprodxoc = jdbcTemplate.query(SQL,new Object[] { UUID.fromString(id) ,UUID.fromString(productoxOC.getIn_idproductoxoc()) }, this::mapParam3);
				}catch (EmptyResultDataAccessException e) {
					e.printStackTrace();
				}
				
				SQL = "select * from oc.t_subitemxprodxoc where in_idoc = ? and in_idproductoxoc = ?";
				List<SubitemxProductoxOC> l_subitemxprodxoc = null;
				try {
					l_subitemxprodxoc = jdbcTemplate.query(SQL,new Object[] { UUID.fromString(id) ,UUID.fromString(productoxOC.getIn_idproductoxoc()) }, this::mapParam6);
				}catch(EmptyResultDataAccessException e){
					e.printStackTrace();
				}
				
				if (l_subitemxprodxoc != null && l_subitemxprodxoc.size()>0) {
					
					for(SubitemxProductoxOC subitemxProductoxOC : l_subitemxprodxoc) {
						SQL = "select * from oc.t_atributoxsubitemxprodxoc where in_idsubitemxprodxoc = ?";
						List<AtributoxSubitemxProductoxOC> l_atxsubitem = null;
						try {
							l_atxsubitem = jdbcTemplate.query(SQL,new Object[] { UUID.fromString(subitemxProductoxOC.getIn_idsubitemxprodxoc()) }, this::mapParam7);
						}catch(EmptyResultDataAccessException e){
							e.printStackTrace();
						}
						subitemxProductoxOC.setLista_atributoxsubitem(l_atxsubitem);
					}
				}
				productoxOC.setLista_atributoxprod(l_atxprodxoc);
				productoxOC.setLista_subitemxprod(l_subitemxprodxoc);
			}
		}
		
		
		return l_prodxoc;
	}
	
	
	
	
	
	
	public Ordencompra mapParam(ResultSet rs, int i) throws SQLException {
		Ordencompra ordencompra = new Ordencompra();

		/*ordencompra.setRucproveedor(StringUtils.trimToEmpty(rs.getString("vc_rucproveedor")));
		ordencompra.setFechacreacion(StringUtils.trimToEmpty(rs.getString("ts_fechacreacion")));
		ordencompra.setNumseguimiento(StringUtils.trimToEmpty(rs.getString("ch_numeroseguimiento")));
		ordencompra.setVc_estadoproveedor(StringUtils.trimToEmpty(rs.getString("vc_estadoproveedor")));
		ordencompra.setVc_estadocomprador(StringUtils.trimToEmpty(rs.getString("vc_estadocomprador")));
		ordencompra.setMoneda(StringUtils.trimToEmpty(rs.getString("vc_moneda")));
        ordencompra.setValortotal(rs.getBigDecimal("fl_valortotal"));
		ordencompra.setValorventa(rs.getBigDecimal("fl_valorventa"));
		ordencompra.setSubtotal(rs.getBigDecimal("fl_subtotal"));        
		ordencompra.setImpuestos(rs.getInt("fl_impuestos"));
		ordencompra.setComentarioproveedor(StringUtils.trimToEmpty(rs.getString("vc_comentarioproveedor")));
		// hasta crear la vista
		ordencompra.setTipooc(rs.getString("vc_tipooc")); 
		//ordencompra.setTipooc("Materiales");
        ordencompra.setIdoc(StringUtils.trimToEmpty(rs.getString("in_idoc")));
		ordencompra.setIdusuariocomprador(StringUtils.trimToEmpty(rs.getString("in_idusuariocomprador")));
		ordencompra.setIdusuarioproveedor(StringUtils.trimToEmpty(rs.getString("in_idusuarioproveedor")));
		ordencompra.setN_usuariocomprador(StringUtils.trimToEmpty(rs.getString("vc_nombreusuariocomprador")));
		ordencompra.setN_usuarioproveedor(StringUtils.trimToEmpty(rs.getString("vc_nombreusuarioproveedor")));
		ordencompra.setVersion(rs.getInt("in_version"));
		ordencompra.setPrioridad(StringUtils.trimToEmpty(rs.getString("ch_prioridad")));
		ordencompra.setIdqt(StringUtils.trimToEmpty(rs.getString("in_idqt")));
		ordencompra.setFecharegistro(StringUtils.trimToEmpty(rs.getString("ts_fecharegistro")));
		ordencompra.setLugarentrega(StringUtils.trimToEmpty(rs.getString("vc_lugarentrega")));
		ordencompra.setTerminosentrega(StringUtils.trimToEmpty(rs.getString("vc_terminosentrega")));
		ordencompra.setTerminos(StringUtils.trimToEmpty(rs.getString("vc_terminos")));
		ordencompra.setConsignatario(StringUtils.trimToEmpty(rs.getString("vc_consignatario")));
		ordencompra.setFechaentrega(StringUtils.trimToEmpty(rs.getString("ts_fechaentrega")));
		ordencompra.setFechaenvio(StringUtils.trimToEmpty(rs.getString("ts_fechaenvio")));
		ordencompra.setPaisembarque(StringUtils.trimToEmpty(rs.getString("vc_paisembarque")));
		ordencompra.setRegionembarque(StringUtils.trimToEmpty(rs.getString("vc_regionembarque")));
		ordencompra.setCondicionembarque(StringUtils.trimToEmpty(rs.getString("vc_condicionembarque")));
		ordencompra.setEmbarcador(StringUtils.trimToEmpty(rs.getString("vc_embarcador")));
		ordencompra.setEmbarqueparcial(rs.getInt("in_embarqueparcial"));
		ordencompra.setPuertodesembarque(StringUtils.trimToEmpty(rs.getString("vc_puertodesembarque")));
		ordencompra.setPolizaseguro(StringUtils.trimToEmpty(rs.getString("vc_polizaseguro")));
		ordencompra.setAduana(StringUtils.trimToEmpty(rs.getString("vc_aduana")));
		ordencompra.setRequiereinspeccion(StringUtils.trimToEmpty(rs.getString("vc_requiereinspeccion")));
		ordencompra.setCompaniainspectora(StringUtils.trimToEmpty(rs.getString("vc_companiainspectora")));
		ordencompra.setNum_inspeccion(StringUtils.trimToEmpty(rs.getString("ch_numeroinspeccion")));
		ordencompra.setCargo(StringUtils.trimToEmpty(rs.getString("vc_cargo")));
		ordencompra.setTiemporecordatorio(rs.getInt("in_tiemporecordatorio"));
		ordencompra.setFecha_autorizacion(StringUtils.trimToEmpty(rs.getString("ts_fechaautorizacion")));
		ordencompra.setComentario(StringUtils.trimToEmpty(rs.getString("vc_comentario")));
		ordencompra.setPreciofob(rs.getBigDecimal("fl_preciofob"));
		ordencompra.setFechaembarque(StringUtils.trimToEmpty(rs.getString("ts_fechaembarque")));
		ordencompra.setCodigoalmacenenvio(StringUtils.trimToEmpty(rs.getString("ch_codigoalmacenenvio")));
		ordencompra.setLab(StringUtils.trimToEmpty(rs.getString("vc_lab")));
		ordencompra.setForwader(StringUtils.trimToEmpty(rs.getString("vc_forwarder")));
		ordencompra.setIsc(rs.getBigDecimal("fl_isc"));
		ordencompra.setFlagorigen(rs.getInt("in_flagorigen"));
		ordencompra.setCampo1(StringUtils.trimToEmpty(rs.getString("vc_campo1")));
		ordencompra.setCampo2(StringUtils.trimToEmpty(rs.getString("vc_campo2")));
		ordencompra.setCampo3(StringUtils.trimToEmpty(rs.getString("vc_campo3")));
		ordencompra.setCampo4(StringUtils.trimToEmpty(rs.getString("vc_campo4")));
		ordencompra.setCampo5(StringUtils.trimToEmpty(rs.getString("vc_campo5")));
		ordencompra.setCampo6(rs.getString("vc_campo6"));
		ordencompra.setCampo7(StringUtils.trimToEmpty(rs.getString("vc_campo7")));
		ordencompra.setCampo8(StringUtils.trimToEmpty(rs.getString("vc_campo8")));		
		ordencompra.setCodigousuariocreacion(StringUtils.trimToEmpty(rs.getString("in_codigousuariocreacion")));
		ordencompra.setCodigousuariomodificacion(StringUtils.trimToEmpty(rs.getString("in_codigousuariomodificacion")));
		//ordencompra.setFechahoracreacion(StringUtils.trimToEmpty(rs.getString("ts_fechahoracreacion")));
		//ordencompra.setFechahoramodificacion(StringUtils.trimToEmpty(rs.getString("ts_fechahoramodificacion")));
		ordencompra.setHabilitado(rs.getInt("in_habilitado"));
		ordencompra.setObsestado(StringUtils.trimToEmpty(rs.getString("vc_obsestado")));
		ordencompra.setTipotransporte(StringUtils.trimToEmpty(rs.getString("vc_tipotransporte")));
		ordencompra.setTasacambio(rs.getBigDecimal("fl_tasacambio"));
		ordencompra.setChequepara(StringUtils.trimToEmpty(rs.getString("vc_chequepara")));
		ordencompra.setIdalmacenenvio(rs.getInt("in_codalmacenenvio"));
		ordencompra.setCodalmacenenvio(StringUtils.trimToEmpty(rs.getString("ch_codigoalmacenenvio")));
		ordencompra.setNarrativa(StringUtils.trimToEmpty(rs.getString("vc_narrativa")));
		ordencompra.setCliente(StringUtils.trimToEmpty(rs.getString("vc_cliente")));
		ordencompra.setTipoprecio(StringUtils.trimToEmpty(rs.getString("vc_tipoprecio")));
		ordencompra.setOtrosimpuestos(rs.getInt("in_otrosimpuestos"));
		ordencompra.setFechamodoc(StringUtils.trimToEmpty(rs.getString("ts_fechamodoc")));
		ordencompra.setFlagmodoc(StringUtils.trimToEmpty(rs.getString("in_flagmodoc")));
		ordencompra.setNumqtprov(StringUtils.trimToEmpty(rs.getString("vc_numqtprov")));
		ordencompra.setMsgmail(StringUtils.trimToEmpty(rs.getString("vc_msgmail")));
		ordencompra.setIdoperadorlog(StringUtils.trimToEmpty(rs.getString("in_idoperadorlog")));
		ordencompra.setEstadob2seller(StringUtils.trimToEmpty(rs.getString("ch_estadob2seller")));
		ordencompra.setIdorgcompradora(StringUtils.trimToEmpty(rs.getString("in_idorganizacioncompradora")));
		//ordencompra.setIdorgproveedora(StringUtils.trimToEmpty(rs.getString("in_idorganizacionproveedora")));
		//ordencompra.setPrecioqt(StringUtils.trimToEmpty(rs.getString("vc_precioqt")));
		ordencompra.setNumerorfq(StringUtils.trimToEmpty(rs.getString("ch_numerorfq")));
		ordencompra.setNombrerfq(StringUtils.trimToEmpty(rs.getString("vc_nombrerfq")));
		ordencompra.setNumeroqt(StringUtils.trimToEmpty(rs.getString("ch_numeroqt")));
		ordencompra.setVersionqt(rs.getInt("in_versionqt"));
		ordencompra.setPagaraproveedor(StringUtils.trimToEmpty(rs.getString("vc_pagaraproveedor")));
		ordencompra.setCod_proveedorcambioestado(StringUtils.trimToEmpty(rs.getString("in_codigoproveedorcambioestado")));
		ordencompra.setDescuentoporcentaje(rs.getBigDecimal("fl_descuentoporcentaje"));
		ordencompra.setImpuestosporcentaje(rs.getBigDecimal("fl_impuestosporcentaje"));
		ordencompra.setFechavisualizacion(StringUtils.trimToEmpty(rs.getString("ts_fechavisualizacion")));
		ordencompra.setMonmig(rs.getString(StringUtils.trimToEmpty("vc_monmig")));
		ordencompra.setLogo(rs.getString(StringUtils.trimToEmpty("vc_logo")));
		ordencompra.setFirma(rs.getString(StringUtils.trimToEmpty("vc_firma")));
		ordencompra.setFlagocdirecta(rs.getInt("in_flagocdirecta"));
		ordencompra.setIn_requiereinspeccion(rs.getInt("in_requiereinspeccion"));
		ordencompra.setIn_numeroap(rs.getInt("in_numeroap"));
		ordencompra.setFl_precioqt(rs.getBigDecimal("fl_precioqt"));
		ordencompra.setVersioncliente(StringUtils.trimToEmpty(rs.getString("vc_versioncliente")));
		ordencompra.setApnumber(StringUtils.trimToEmpty(rs.getString("vc_apnumber")));		
		ordencompra.setCoderror(StringUtils.trimToEmpty(rs.getString("vc_coderror")));
		ordencompra.setDescerror(StringUtils.trimToEmpty(rs.getString("vc_descerror")));
		ordencompra.setFechadocumentooc(StringUtils.trimToEmpty(rs.getString("ts_fechadocumentooc")));
		ordencompra.setIdorgpropietaria(StringUtils.trimToEmpty(rs.getString("in_idorgpropietaria")));
		ordencompra.setTs_fechanotificacion(StringUtils.trimToEmpty(rs.getString("ts_fechanotificacion")));
		ordencompra.setEnviarfactura(StringUtils.trimToEmpty(rs.getString("vc_enviarfacturaa")));
		ordencompra.setIgv(rs.getBigDecimal("de_iva"));
		ordencompra.setUtilidades(rs.getBigDecimal("de_utilidades"));
		ordencompra.setGastos_generales(rs.getBigDecimal("de_gastosgen"));
		ordencompra.setVc_forwarder(StringUtils.trimToEmpty(rs.getString("vc_forwarder")));

		System.out.println("ORIGEN DATOS-> "+ origen);
		if(origen == 1) { //B2M
			System.out.println("this.lista_origen1.get(1) "+ origen);
			ordencompra.setRuc_comprador(StringUtils.trimToEmpty(rs.getString("vc_ruccomprador")));
			ordencompra.setRazonsocialproveedor(StringUtils.trimToEmpty(rs.getString("vc_razonsocialproveedor")));
			ordencompra.setAtenciona(StringUtils.trimToEmpty(rs.getString("vc_atenciona")));
			ordencompra.setEmailcontacto(StringUtils.trimToEmpty(rs.getString("vc_emailcontacto")));
			ordencompra.setFactura(StringUtils.trimToEmpty(rs.getString("vc_facturara")));
			ordencompra.setCondicionpago(StringUtils.trimToEmpty(rs.getString("vc_condicionpago"))); // parece que solo usa condicion de pago
			ordencompra.setValorcondpago(rs.getBigDecimal("fl_valorcondpago"));
			ordencompra.setUnidcondpago(StringUtils.trimToEmpty(rs.getString("vc_unidadcondpago")));
			ordencompra.setDireccionfactura(StringUtils.trimToEmpty(rs.getString("vc_direccionfactura")));
			ordencompra.setAutorizadopor(StringUtils.trimToEmpty(rs.getString("vc_autorizadopor")));
			
			// a reemplazar luego de confirmar en bd
			ordencompra.setVc_razonsocialcomprador("Grupo Centenario S.A");
			ordencompra.setVc_("Usuario de Prueba");
			ordencompra.setIn_codigoproveedor(rs.getString("in_idorganizacionproveedora"));
			ordencompra.setVc_grupocompra("Grupo de compra prueba");
			ordencompra.setTs_fechainiciocontrato(StringUtils.trimToEmpty(rs.getString("ts_fechahoracreacion")));
	
		}else if(origen == 0) {// Eseace
			System.out.println("this.lista_origen1.get(0) "+ origen);
			ordencompra.setVc_objetocontratacion(StringUtils.trimToEmpty(rs.getString("vc_objetocontratacion")));
			ordencompra.setVc_tipocontratacion(StringUtils.trimToEmpty(rs.getString("vc_tipocontratacion")));
			ordencompra.setVc_numeroexpedientesiaf(StringUtils.trimToEmpty(rs.getString("vc_numeroexpedientesiaf")));
			ordencompra.setTs_fechacompromisoppto(StringUtils.trimToEmpty(rs.getString("ts_fechacompromisoppto")));
			ordencompra.setTipocambio(rs.getBigDecimal("fl_tipocambio"));
			ordencompra.setTipo(rs.getInt("in_tipo"));
			ordencompra.setVc_descripcioncontratacion(rs.getString(StringUtils.trimToEmpty("vc_descripcioncontratacion")));
			ordencompra.setVc_unidadessolicitantes(StringUtils.trimToEmpty(rs.getString("vc_unidadessolicitantes")));
			ordencompra.setVc_unidadesinformantes(StringUtils.trimToEmpty(rs.getString("vc_unidadesinformantes")));
			ordencompra.setComentariocomprador(StringUtils.trimToEmpty(rs.getString("vc_comentariocomprador")));
			ordencompra.setOtroscostos(rs.getBigDecimal("fl_otroscostos"));
			//ordencompra.setDescuento(rs.getBigDecimal("fl_descuento"));
		}*/////////////////////////////////////////////////////////////////////////////////////////////////////////////
		
		ordencompra.setNumseguimiento(StringUtils.trimToEmpty(rs.getString("ch_numeroseguimiento")));
		ordencompra.setVc_estadoproveedor(StringUtils.trimToEmpty(rs.getString("vc_estadoproveedor")));
		ordencompra.setVc_estadocomprador(StringUtils.trimToEmpty(rs.getString("vc_estadocomprador")));
		ordencompra.setAtenciona(StringUtils.trimToEmpty(rs.getString("vc_atenciona")));
		ordencompra.setRucproveedor(StringUtils.trimToEmpty(rs.getString("vc_rucproveedor")));
		ordencompra.setRuc_comprador(StringUtils.trimToEmpty(rs.getString("vc_ruccomprador")));
		ordencompra.setRazonsocialproveedor(StringUtils.trimToEmpty(rs.getString("vc_razonsocialproveedor")));
		ordencompra.setVc_razonsocialcomprador(StringUtils.trimToEmpty(rs.getString("vc_razonsocialcomprador")));
		ordencompra.setIn_codigoproveedor(StringUtils.trimToEmpty(rs.getString("vc_codinternoprov")));
		ordencompra.setVersion(rs.getInt("in_version"));
		//----->falta agregar a vm<-----//
		ordencompra.setTs_fechacreacion(StringUtils.trimToEmpty(rs.getString("ts_fechacreacion")));
		ordencompra.setEmailcontacto(StringUtils.trimToEmpty(rs.getString("vc_emailcontacto")));
		ordencompra.setVc_enviarfacturaa(StringUtils.trimToEmpty(rs.getString("vc_enviarfacturaa")));
		ordencompra.setVc_facturara(StringUtils.trimToEmpty(rs.getString("vc_facturara")));
		ordencompra.setVc_grupocompra(StringUtils.trimToEmpty(rs.getString("vc_grupocompra")));
		ordencompra.setMoneda(StringUtils.trimToEmpty(rs.getString("vc_moneda")));
		ordencompra.setCondicionpago(StringUtils.trimToEmpty(rs.getString("vc_condicionpago")));
		ordencompra.setTs_fechainiciocontrato(StringUtils.trimToEmpty(rs.getString("ts_fechainicontrato")));
		ordencompra.setTs_fechafincontrato(StringUtils.trimToEmpty(rs.getString("ts_fechafincontrato")));
		ordencompra.setAutorizadopor(StringUtils.trimToEmpty(rs.getString("vc_autorizadopor")));
		ordencompra.setValorventa(rs.getBigDecimal("fl_valorventa"));
		ordencompra.setSubtotal(rs.getBigDecimal("fl_subtotal"));        
		ordencompra.setImpuestos(rs.getBigDecimal("fl_impuestos"));
		ordencompra.setValortotal(rs.getBigDecimal("fl_valortotal"));
		ordencompra.setDireccionfactura(StringUtils.trimToEmpty(rs.getString("vc_direccionfactura")));
		ordencompra.setAtenciona(StringUtils.trimToEmpty(rs.getString("vc_atenciona")));
		ordencompra.setTerminos(StringUtils.trimToEmpty(rs.getString("vc_terminos")));
		ordencompra.setAutorizadopor(StringUtils.trimToEmpty(rs.getString("vc_autorizadopor")));
		ordencompra.setNarrativa(StringUtils.trimToEmpty(rs.getString("vc_narrativa")));
		ordencompra.setComentario(StringUtils.trimToEmpty(rs.getString("vc_comentario")));
		ordencompra.setComentarioproveedor(StringUtils.trimToEmpty(rs.getString("vc_comentarioproveedor")));
		ordencompra.setComentariocomprador(StringUtils.trimToEmpty(rs.getString("vc_comentariocomprador")));
		ordencompra.setVc_creadopor(StringUtils.trimToEmpty(rs.getString("vc_nombreusuariocreadopor")));
		ordencompra.setN_usuariocomprador(StringUtils.trimToEmpty(rs.getString("vc_nombreusuariocomprador")));
		//ordencompra.setVc_("Usuario de Prueba");
		ordencompra.setImpuestosporcentaje(rs.getBigDecimal("fl_impuestosporcentaje"));
		//////////////////////////////////
		switch(StringUtils.trimToEmpty(rs.getString("vc_tipooc"))) {
			case "M":
				ordencompra.setTipooc("Material");
				break;
			case "S":
				ordencompra.setTipooc("Servicio");
				break;
			default:
				ordencompra.setTipooc("Caso de Prueba");
				break;
		}
		ordencompra.setVc_condicionesgenerales(StringUtils.trimToEmpty(rs.getString("vc_condicionesgenerales")));
		
		
		return ordencompra;
	}
	
	public ProductoxOC mapParam2(ResultSet rs, int i) throws SQLException {
		ProductoxOC productoxOC = new ProductoxOC();

		productoxOC.setIn_idproductoxoc(StringUtils.trimToEmpty(rs.getString("in_idproductoxoc")));
		productoxOC.setIn_posicion(rs.getInt("in_posicion"));
		//productoxOC.setVc_posicion(StringUtils.trimToEmpty(rs.getString("vc_posicion")));
		productoxOC.setFl_cantidadproducto(rs.getBigDecimal("fl_cantidadproducto"));
		productoxOC.setIn_idunidadproducto(rs.getInt("in_idunidadproducto")); // no debe ser solo el id
		productoxOC.setFl_precioproducto(rs.getBigDecimal("fl_precioproducto"));
		productoxOC.setFl_preciototalproducto(rs.getBigDecimal("fl_preciototalproducto"));
		productoxOC.setTs_fechaentregaproducto(StringUtils.trimToEmpty(rs.getString("ts_fechaentregaproducto")));
		productoxOC.setVc_deslargapro(StringUtils.trimToEmpty(rs.getString("vc_deslargapro")));

		productoxOC.setVc_codigoproductoorg(StringUtils.trimToEmpty(rs.getString("vc_codigoproductoorg")));
		productoxOC.setIn_tipoproducto(rs.getInt("in_tipoproducto"));
		productoxOC.setVc_descortapro(StringUtils.trimToEmpty(rs.getString("vc_descortapro")));
		productoxOC.setIn_codigousuariocreacion(StringUtils.trimToEmpty(rs.getString("in_idusuariocreacion")));
		productoxOC.setIn_habilitado(rs.getInt("in_habilitado"));
		//se comenta para reutilizar en preReg Guia
		//productoxOC.setVc_estadoprod(StringUtils.trimToEmpty(rs.getString("ch_estadoprod")));
		/***para pre guia *******************************************************************/
		System.out.println("VALORES CARGADOS EN LA CLASE  ****************************** " );
		System.out.println("origen : " +origen);
		System.out.println("cabecera : " +cabecera);	
		System.out.println("solo_productos : " +solo_productos);	
		System.out.println("perfil : " +perfil);	
		
			String estadop = StringUtils.trimToEmpty(rs.getString("ch_estadoprod"));
		       if(solo_productos==0 || solo_productos==-1){
					productoxOC.setVc_estadoprod(estadop);
				}else{
						if(estadop.equalsIgnoreCase("ODESP")){
							productoxOC.setVc_estadoprod("Despachada");
						}else{
							if(estadop.equalsIgnoreCase("OACEP")){
								productoxOC.setVc_estadoprod("Aceptada");
							}else{
								productoxOC.setVc_estadoprod(estadop);
							}
						}		
				}
				
		/**********************************************************************************/
		
		productoxOC.setIn_idorganizacion(StringUtils.trimToEmpty(rs.getString("in_idorganizacion")));
		productoxOC.setIn_idproducto(StringUtils.trimToEmpty(rs.getString("in_idproducto")));
		productoxOC.setIn_idoc(StringUtils.trimToEmpty(rs.getString("in_idoc")));
		productoxOC.setIn_idproductocat(rs.getInt("in_idproductocat"));
		productoxOC.setIn_idcategoria(rs.getInt("in_idcategoria"));
		productoxOC.setVc_campo1(StringUtils.trimToEmpty(rs.getString("vc_campo1")));
		productoxOC.setVc_campo2(StringUtils.trimToEmpty(rs.getString("vc_campo2")));
		productoxOC.setVc_campo3(StringUtils.trimToEmpty(rs.getString("vc_campo3")));
		productoxOC.setVc_campo4(StringUtils.trimToEmpty(rs.getString("vc_campo4")));
		productoxOC.setVc_campo5(StringUtils.trimToEmpty(rs.getString("vc_campo5")));
		productoxOC.setIn_idusuariomodificacion(StringUtils.trimToEmpty(rs.getString("in_idusuariomodificacion")));
		productoxOC.setTs_fechahoracreacion(StringUtils.trimToEmpty(rs.getString("ts_fechahoracreacion")));
		productoxOC.setTs_fechahoramodificacion(StringUtils.trimToEmpty(rs.getString("ts_fechahoramodificacion")));
		productoxOC.setIn_itemrequisicion(rs.getInt("in_itemrequisicion"));
		productoxOC.setFl_cantidadproductopend(rs.getBigDecimal("fl_cantidadproductopend"));
		productoxOC.setVc_numeroparte(StringUtils.trimToEmpty(rs.getString("vc_numeroparte")));
		productoxOC.setIn_idprodb2m(rs.getInt("in_idprodb2m"));
		//productoxOC.setVc_itemrequisicion(StringUtils.trimToEmpty(rs.getString("vc_itemrequisicion")));
		//productoxOC.setVc_partnumber(StringUtils.trimToEmpty(rs.getString("vc_partnumber")));
		productoxOC.setIn_idrequerimiento(rs.getInt("in_idrequerimiento"));
		productoxOC.setIn_posicionreq(rs.getInt("in_posicionreq"));
		productoxOC.setVc_commodity(StringUtils.trimToEmpty(rs.getString("ch_commodity")));
		productoxOC.setFl_cantidadrecepcionada(rs.getBigDecimal("fl_cantidadrecepcionada"));	
		productoxOC.setVc_unidadproducto(StringUtils.trimToEmpty(rs.getString("vc_unidadproducto")));
		productoxOC.setDe_impuestos(rs.getBigDecimal("de_impuestos"));
		productoxOC.setVc_idtablaunidad(StringUtils.trimToEmpty(rs.getString("vc_idtablaunidad")));
		productoxOC.setVc_idregistrounidad(StringUtils.trimToEmpty(rs.getString("vc_idregistrounidad")));
		if (productoxOC.getFl_cantidadproducto()!= null){
			if (productoxOC.getFl_cantidadproductopend()!=null)
				productoxOC.setFl_cantidaddespachada(productoxOC.getFl_cantidadproducto().subtract(productoxOC.getFl_cantidadproductopend()));
			else productoxOC.setFl_cantidaddespachada(productoxOC.getFl_cantidadproducto());
		}
		return productoxOC;
	}
	
	public AtributoxProductoxOC mapParam3(ResultSet rs, int i) throws SQLException {
		AtributoxProductoxOC atributoxProductoxOC = new AtributoxProductoxOC();
		
		atributoxProductoxOC.setIn_idatributo(StringUtils.trimToEmpty(rs.getString("in_idatributo")));
		atributoxProductoxOC.setVc_valorenviado(StringUtils.trimToEmpty(rs.getString("vc_valorenviado")));
		atributoxProductoxOC.setVc_nombreunidad(StringUtils.trimToEmpty(rs.getString("vc_nombreunidad")));
		atributoxProductoxOC.setVc_idoperador(StringUtils.trimToEmpty(rs.getString("vc_idoperador")));
		atributoxProductoxOC.setVc_nombreatributo(StringUtils.trimToEmpty(rs.getString("vc_nombreatributo")));
		atributoxProductoxOC.setIn_codigousuariocreacion(StringUtils.trimToEmpty(rs.getString("in_codigousuariocreacion")));
		atributoxProductoxOC.setIn_codigousuariomodificacion(StringUtils.trimToEmpty(rs.getString("in_codigousuariomodificacion")));
		atributoxProductoxOC.setIn_habilitado(rs.getInt("in_habilitado"));		
		
		return atributoxProductoxOC;
	}
	
	public AtributoxOC mapParam4(ResultSet rs, int i) throws SQLException {
		AtributoxOC atributoxOC = new AtributoxOC();
		
		atributoxOC.setVc_nombreatributo(StringUtils.trimToEmpty(rs.getString("vc_nombreatributo")));
		atributoxOC.setIn_idatributo(StringUtils.trimToEmpty(rs.getString("in_idatributo")));
		atributoxOC.setVc_valorenviado(StringUtils.trimToEmpty(rs.getString("vc_valorenviado")));
		atributoxOC.setVc_mandatorio(StringUtils.trimToEmpty(rs.getString("ch_mandatorio")));
		atributoxOC.setVc_modificable(StringUtils.trimToEmpty(rs.getString("ch_modificable")));
		atributoxOC.setIn_idunidad(rs.getInt("in_idunidad"));
		atributoxOC.setIn_codigousuariocreacion(StringUtils.trimToEmpty(rs.getString("in_codigousuariocreacion")));
		atributoxOC.setIn_codigousuariomodificacion(StringUtils.trimToEmpty(rs.getString("in_codigousuariomodificacion")));
		atributoxOC.setIn_habilitado(rs.getInt("in_habilitado"));		
		
		return atributoxOC;
	}
	
	
	public ArchivoxOC mapParam5(ResultSet rs, int i) throws SQLException {
		ArchivoxOC archivoxOC = new ArchivoxOC();
		
		archivoxOC.setIn_idarchivooc(StringUtils.trimToEmpty(rs.getString("in_idarchivooc")));
		archivoxOC.setVc_nombre(StringUtils.trimToEmpty(rs.getString("vc_nombre")));
		archivoxOC.setVc_ruta(StringUtils.trimToEmpty(rs.getString("vc_ruta")));
		archivoxOC.setVc_descripcion(StringUtils.trimToEmpty(rs.getString("vc_descripcion")));
		archivoxOC.setIn_codigousuariocreacion(StringUtils.trimToEmpty(rs.getString("in_codigousuariocreacion")));
		archivoxOC.setIn_codigousuariomodificacion(StringUtils.trimToEmpty(rs.getString("in_codigousuariomodificacion")));
		archivoxOC.setIn_habilitado(rs.getInt("in_habilitado"));
		
		
		return archivoxOC;
	}
	
	public SubitemxProductoxOC mapParam6(ResultSet rs, int i) throws SQLException{
		SubitemxProductoxOC subitemxProductoxOC = new SubitemxProductoxOC();
		
		subitemxProductoxOC.setIn_idsubitemxprodxoc(StringUtils.trimToEmpty(rs.getString("in_idsubitemxprodxoc")));
		subitemxProductoxOC.setVc_codigosubitemorg(StringUtils.trimToEmpty(rs.getString("vc_codigosubitemorg")));
		subitemxProductoxOC.setInt_tiposubitem(rs.getInt("in_tiposubitem"));
		subitemxProductoxOC.setVc_descortasubitem(StringUtils.trimToEmpty(rs.getString("vc_descortasubitem")));
		subitemxProductoxOC.setVc_desclargasubitem(StringUtils.trimToEmpty(rs.getString("vc_deslargasubitem")));
		subitemxProductoxOC.setVc_campo1(StringUtils.trimToEmpty(rs.getString("vc_campo1")));
		subitemxProductoxOC.setVc_campo2(StringUtils.trimToEmpty(rs.getString("vc_campo2")));
		subitemxProductoxOC.setVc_campo3(StringUtils.trimToEmpty(rs.getString("vc_campo3")));
		subitemxProductoxOC.setVc_campo4(StringUtils.trimToEmpty(rs.getString("vc_campo4")));
		subitemxProductoxOC.setVc_campo5(StringUtils.trimToEmpty(rs.getString("vc_campo5")));
		subitemxProductoxOC.setIn_idusuariocreacion(StringUtils.trimToEmpty(rs.getString("in_idusuariocreacion")));
		subitemxProductoxOC.setIn_idusuariomodificacion(StringUtils.trimToEmpty(rs.getString("in_idusuariomodificacion")));
		subitemxProductoxOC.setTs_fechahoracreacion(StringUtils.trimToEmpty(rs.getString("ts_fechahoracreacion")));
		subitemxProductoxOC.setTs_fechahoramodificacion(StringUtils.trimToEmpty(rs.getString("ts_fechahoramodificacion")));
		subitemxProductoxOC.setIn_habilitado(rs.getInt("in_habilitado"));
		subitemxProductoxOC.setVc_estadosubitem(StringUtils.trimToEmpty(rs.getString("vc_estadosubitem")));
		subitemxProductoxOC.setIn_idorganizacion(StringUtils.trimToEmpty(rs.getString("in_idorganizacion")));
		subitemxProductoxOC.setIn_subitemposicion(rs.getInt("in_subitemposicion"));
		subitemxProductoxOC.setTs_fechaentregasubitem(StringUtils.trimToEmpty(rs.getString("ts_fechaentregasubitem")));
		subitemxProductoxOC.setFl_preciosubitem(rs.getBigDecimal("fl_preciosubitem"));
		subitemxProductoxOC.setFl_cantidadsubitem(rs.getBigDecimal("fl_cantidadsubitem"));
		subitemxProductoxOC.setFl_preciototalsubitem(rs.getBigDecimal("fl_preciototalsubitem"));
		subitemxProductoxOC.setIn_idunidadsubitem(rs.getInt("in_idunidadsubitem"));
		subitemxProductoxOC.setFl_cantidadsubitempend(rs.getBigDecimal("fl_cantidadsubitempend"));
		subitemxProductoxOC.setVc_numeroparte(StringUtils.trimToEmpty(rs.getString("vc_numeroparte")));
		subitemxProductoxOC.setVc_subitemposicion(StringUtils.trimToEmpty(rs.getString("vc_subitemposicion")));
		subitemxProductoxOC.setIn_idrequerimiento(rs.getInt("in_idrequerimiento"));
		subitemxProductoxOC.setIn_posicionreq(rs.getInt("in_posicionreq"));
		subitemxProductoxOC.setVc_nombreunidad(StringUtils.trimToEmpty(rs.getString("vc_nombreunidad")));
		
		return subitemxProductoxOC;
	}
	
	public AtributoxSubitemxProductoxOC mapParam7(ResultSet rs, int i) throws SQLException{
		AtributoxSubitemxProductoxOC atributoxSubitemxProductoxOC = new AtributoxSubitemxProductoxOC();
		
		atributoxSubitemxProductoxOC.setIn_idatributoxsubitemxprodxoc(StringUtils.trimToEmpty(rs.getString("in_idatributoxsubitemxprodxoc")));
		atributoxSubitemxProductoxOC.setVc_valorenviado(StringUtils.trimToEmpty(rs.getString("vc_valorenviado")));
		atributoxSubitemxProductoxOC.setVc_mandatorio(StringUtils.trimToEmpty(rs.getString("vc_mandatorio")));
		atributoxSubitemxProductoxOC.setVc_modificable(StringUtils.trimToEmpty(rs.getString("vc_modificable")));
		//atributoxSubitemxProductoxOC.setIn_idunidad(rs.getInt("in_idunidad"));
		atributoxSubitemxProductoxOC.setVc_nombreunidad(StringUtils.trimToEmpty(rs.getString("vc_nombreunidad")));
		atributoxSubitemxProductoxOC.setVc_idoperador(StringUtils.trimToEmpty(rs.getString("vc_idoperador")));
		atributoxSubitemxProductoxOC.setVc_unidaderror(StringUtils.trimToEmpty(rs.getString("vc_unidaderror")));
		atributoxSubitemxProductoxOC.setVc_nombreatributo(StringUtils.trimToEmpty(rs.getString("vc_nombreatributo")));
		atributoxSubitemxProductoxOC.setVc_campo1(StringUtils.trimToEmpty(rs.getString("vc_campo1")));
		atributoxSubitemxProductoxOC.setVc_campo2(StringUtils.trimToEmpty(rs.getString("vc_campo2")));
		atributoxSubitemxProductoxOC.setVc_campo3(StringUtils.trimToEmpty(rs.getString("vc_campo3")));
		atributoxSubitemxProductoxOC.setVc_campo4(StringUtils.trimToEmpty(rs.getString("vc_campo4")));
		atributoxSubitemxProductoxOC.setVc_campo5(StringUtils.trimToEmpty(rs.getString("vc_campo5")));
		atributoxSubitemxProductoxOC.setIn_codigousuariocreacion(StringUtils.trimToEmpty(rs.getString("in_codigousuariocreacion")));
		atributoxSubitemxProductoxOC.setIn_codigousuariomodificacion(StringUtils.trimToEmpty(rs.getString("in_codigousuariomodificacion")));
		atributoxSubitemxProductoxOC.setTs_fechahoracreacion(StringUtils.trimToEmpty(rs.getString("ts_fechahoracreacion")));
		atributoxSubitemxProductoxOC.setTs_fechahoramodificacion(StringUtils.trimToEmpty(rs.getString("ts_fechahoramodificacion")));
		atributoxSubitemxProductoxOC.setIn_habilitado(rs.getInt("in_habilitado"));
		return atributoxSubitemxProductoxOC;
	}
	
	
	public int getOrigen() {
		return origen;
	}

	public void setOrigen(int origen) {
		this.origen = origen;
	}
	
	
	public char getPerfil() {
		return perfil;
	}

	public void setPerfil(char perfil) {
		this.perfil = perfil;
	}
	
	public void setCabecera(int solo_productos) {
		this.cabecera = solo_productos;
	}
	
	public void setSolo_productos(int solo_productos) {
		this.solo_productos = solo_productos;
	}
	
	
}
