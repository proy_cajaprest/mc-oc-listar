package com.ebiz.msoclistar;

import java.util.List;

import com.ebiz.msoclistar.Ordencompra;

public interface DetalleordencompraRepository {

	char getPerfil();
    
    void setPerfil(char perfil);
    
    int getOrigen();
    
    void setOrigen(int origen);
    
    void setCabecera(int cabecera);
    
    void setSolo_productos(int solo_productos);
	
	Ordencompra detalleOrdenCompra(String id);
	
	List<ProductoxOC> detalleProductosOC(String id);
}
