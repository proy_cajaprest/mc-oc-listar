package com.ebiz.msoclistar.controller;

//import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ebiz.msoclistar.DetalleordencompraService;
//import com.ebiz.common.security.resource.PrincipalUtil;
import com.ebiz.msoclistar.OcListarService;
import com.ebiz.msoclistar.RequestDetOC;
import com.ebiz.msoclistar.ResponseListarOc;
import com.ebiz.msoclistar.RequestListarOc;
import com.ebiz.msoclistar.ResponseDetOC;

import java.sql.Date;
//import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.ArrayList;
//import java.time.LocalDateTime;
//import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.UUID;
import java.util.Arrays;
import java.util.Map;

@RestController()
@RequestMapping("/ordenes")
public class OcController {

	@Autowired
	private OcListarService ocService;
	
	@Autowired
	private DetalleordencompraService detalleordencompraService;
	
	private final Logger logger = LoggerFactory.getLogger(OcController.class);
	

	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public ResponseDetOC data(@RequestHeader("tipo_empresa") String tipo_empresa,
	            				          @RequestHeader("origen_datos") String origen_datos,
										  @RequestHeader("Authorization") String authorization,
	                                      @PathVariable("id") String id,
	                                      @RequestParam(value = "cabecera", defaultValue = "1") int cabecera,
	                                      @RequestParam(value = "solo_productos", defaultValue = "0") int solo_productos) {
										  
		//int cabecera = 1;//obtener como parametro cuando sea necesario
		//int solo_productos =0;//obtener como parametro cuando sea necesario
		RequestDetOC requestOC = new RequestDetOC();
		//RequestDetOC requestOC = null;//nuevo
		
		
		System.out.println(origen_datos.toUpperCase());
		System.out.println(tipo_empresa.toLowerCase());

		
		//FILTRO 01
		switch(origen_datos.toUpperCase()) {
			case "PEESEACE":
				logger.info("origen PEESEACE");
				requestOC.setOrigen(0);
				break;
			case "PEB2M":
				logger.info("origen PEB2M");
				requestOC.setOrigen(1);
				break;
			default:
				return null;
		}
		
		//FILTRO 02
		
		if (tipo_empresa.toLowerCase().contains("c")) {
			logger.info(" ES UN COMPRADOR ");
			requestOC.setPerfil('c');
		}
		else if (tipo_empresa.toLowerCase().contains("p")) {
			logger.info(" ES UN PROVEEDOR ");
			requestOC.setPerfil('p');
		}
		else
			return null;
		
		try {
			requestOC.setId(id);
			requestOC.setCabecera(cabecera);
			requestOC.setSolo_productos(solo_productos);
			return detalleordencompraService.data(requestOC);
		}
		
		catch(Exception e) {
			e.printStackTrace();
			System.out.println("ERROR : " + e.getMessage().toString());
			return null;
		}
		
	}
	
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	@ResponseBody
	public ResponseListarOc datatableList(@RequestHeader("tipo_empresa") String tipo_empresa,
									@RequestHeader("origen_datos") String origen_datos,
									@RequestHeader("org_id") UUID org_id,
									@RequestHeader("Authorization") String authorization,
									@RequestParam Map<String, String> params) {
				
		int draw = params.get("draw")==null?0:Integer.parseInt(params.get("draw"));
		int start = params.get("start")==null?0:Integer.parseInt(params.get("start"));
		int length = params.get("length")==null?0:Integer.parseInt(params.get("length"));
		String vc_rucproveedor = params.get("NITVendedor")==null?"":params.get("NITVendedor");
		String vc_razonsocialproveedor = params.get("NombreVendedor")==null?"":params.get("NombreVendedor");
		String vc_ruccomprador = params.get("NITComprador")==null?"":params.get("NITComprador");
		String vc_razonsocialcomprador = params.get("NombreComprador")==null?"":params.get("NombreComprador");
		String vc_numeroseguimiento_rfq = params.get("NumeroRfq")==null?"":params.get("NumeroRfq");
		String vc_numeroseguimiento_oc = params.get("NumeroOrden")==null?"":params.get("NumeroOrden");		
		String vc_estado = params.get("EstadoOrden")==null?"":params.get("EstadoOrden");		
		String org_idcomprador = params.get("org_idComprador")==null?"":params.get("org_idComprador");	
		Date ts_fecharegistro_inicio = params.get("FechaRegistroInicio")==null?null:Date.valueOf(params.get("FechaRegistroInicio"));
		Date ts_fecharegistro_fin = params.get("FechaRegistroFin")==null?null:Date.valueOf(params.get("FechaRegistroFin"));
		List<String> vc_tipooc = params.get("TipoOrden")==null?null:Arrays.asList(params.get("TipoOrden").split(","));
		List<String> column_namestemp = params.get("column_names")==null?null:Arrays.asList(params.get("column_names").split(","));
		
		//logger.info("draw: "+draw+"\nstart: "+start+"\nlength: "+length+"\norder_colum: "+order_colum+"\norder_direc: " + order_direc+"\n");
		logger.info("draw: "+draw+"\nstart: "+start+"\nlength: "+length+"\n");		
		logger.info("FechaRegistroInicio: "+ts_fecharegistro_inicio+"\nFechaRegistroFin: "+ts_fecharegistro_fin);
		List<String> column_names=new ArrayList<String>();
		for (int j=0;j<column_namestemp.size();j++) {
        	String columna = column_namestemp.get(j);
        	switch(columna) {
	        	case "CodigoOrden":
	        		column_names.add("in_idoc"); break;
	        	case "NumeroOrden":
	        		column_names.add("ch_numeroseguimiento"); break;
	        		
	        	case "EstadoOrden":
	        		column_names.add("vc_estado"); break;
	        		
	        	case "TipoOrden":
	        		column_names.add("vc_tipooc"); break;
	        		
	        	case "AtencionA":
	        		column_names.add("vc_atenciona"); break;
	        		
	        	case "NITComprador":
	        		column_names.add("vc_ruccomprador"); break;
	        		
	        	case "UsuarioComprador":
	        		column_names.add("vc_nombreusuariocreadopor"); break;
	        		
	        	case "NombreComprador":
	        		column_names.add("vc_razonsocialcomprador"); break;
	        		
	        	case "NITVendedor":
	        		column_names.add("vc_rucproveedor"); break;
	        		
	        	case "UsuarioProveedor":
	        		column_names.add("vc_nombreusuarioproveedor"); break;
	        		
	        	case "NombreVendedor":
	        		column_names.add("vc_razonsocialproveedor"); break;
	        		
	        	case "ValorTotal":
	        		column_names.add("fl_valortotal"); break;
	        		
	        	case "MonedaOrden":
	        		column_names.add("vc_moneda"); break;
	        		
	        	case "Fecha":
	        		column_names.add("ts_fechacreacion"); break;
	        		
	        	case "NumeroRfq":
	        		column_names.add("ch_numerorfq"); break;
	        		
	        	case "Version":
	        		column_names.add("in_version"); break;
	        		
	        	case "FechaRegistro":
	        		column_names.add("ts_fecharegistro"); break;
        	}
        }
		//
		
		RequestListarOc requestOc = new RequestListarOc();
		// FILTRO 1:
		logger.info("ORIGEN DATOS ---> " + origen_datos);
		switch(origen_datos.toUpperCase()) {
			case "PEESEACE":
				requestOc.setOrigen(0);
				break;
			case "PEB2M":
				requestOc.setOrigen(1);
				break;
			default:
				return null;
		}
		// FILTRO 2
		/*logger.info("AUTHORIZATION ---> " + tipo_empresa);
		if (!tipo_empresa.toLowerCase().contains("bearer"))
			return null;
		else {*/
			
			if (tipo_empresa.toLowerCase().contains("c")) {
				logger.info(" ES UN COMPRADOR ");
				requestOc.setPerfil('c');
			}
			else if (tipo_empresa.toLowerCase().contains("p")) {
				logger.info(" ES UN PROVEEDOR ");
				requestOc.setPerfil('p');
			}
			else {
				logger.info("No hay tipo de empresa --> Nulo");
				return null;
				}
			
			try {
				// seteamos las fechas
				if (ts_fecharegistro_fin == null && ts_fecharegistro_inicio == null) {
					ts_fecharegistro_fin = Date.valueOf(LocalDate.now());
					ts_fecharegistro_inicio = Date.valueOf(ts_fecharegistro_fin.toLocalDate().minusDays(30));
					//ts_fechacreacion_fin = Timestamp.valueOf(LocalDateTime.now());
					//ts_fechacreacion_inicio = Timestamp.valueOf(ts_fechacreacion_fin.toLocalDateTime().minusDays(30).truncatedTo(ChronoUnit.DAYS));
					logger.info("FECHA INICIO AUTOMATICA ----> " + ts_fecharegistro_inicio.toString());
					logger.info("FECHA FIN AUTIMATICA ----> " + ts_fecharegistro_fin.toString());
				}
				else {
					if(ts_fecharegistro_fin == null) {
						ts_fecharegistro_fin = Date.valueOf(ts_fecharegistro_inicio.toLocalDate().plusDays(30));
						//ts_fechacreacion_fin = Timestamp.valueOf(ts_fechacreacion_inicio.toLocalDateTime().plusDays(30));
						logger.info("FECHA FIN AUTOMATICA ----> " + ts_fecharegistro_fin.toString());
					}
					if(ts_fecharegistro_inicio == null) {
						ts_fecharegistro_inicio = Date.valueOf(ts_fecharegistro_fin.toLocalDate().minusDays(30));
						//ts_fechacreacion_inicio = Timestamp.valueOf(ts_fechacreacion_fin.toLocalDateTime().minusDays(30).truncatedTo(ChronoUnit.DAYS));
						logger.info("FECHA INICIO AUTOMATICA ----> " + ts_fecharegistro_inicio.toString());
					}
				}
				requestOc.setColumn_names(column_names);
				requestOc.setOrder_columns(params, tipo_empresa.toUpperCase());
				requestOc.setDraw(draw < 0 ? 0 : draw);
				requestOc.setStart(start < 0 ? 0 : start);
				requestOc.setLength(length);
				requestOc.setVc_rucproveedor(vc_rucproveedor);
				requestOc.setVc_razonsocialproveedor(vc_razonsocialproveedor);
				requestOc.setVc_ruccomprador(vc_ruccomprador);
				requestOc.setVc_razonsocialcomprador(vc_razonsocialcomprador);
				requestOc.setVc_numeroseguimiento_rfq(vc_numeroseguimiento_rfq);
				requestOc.setVc_numeroseguimiento_oc(vc_numeroseguimiento_oc);
				requestOc.setVc_estado(vc_estado);
				requestOc.setTs_fecharegistro_inicio(ts_fecharegistro_inicio);
				requestOc.setTs_fecharegistro_fin(ts_fecharegistro_fin);
				requestOc.setVc_tipooc(vc_tipooc);
				requestOc.setOrg_id(org_id.toString());
				requestOc.setOrg_idcomprador(org_idcomprador);
				
				return ocService.datatableList(requestOc);
			}
			catch(Exception e) {
				e.printStackTrace();
				//logger.error("ERROR : " + e.getMessage().toString());
				return null;
			}	
			
	}
}
