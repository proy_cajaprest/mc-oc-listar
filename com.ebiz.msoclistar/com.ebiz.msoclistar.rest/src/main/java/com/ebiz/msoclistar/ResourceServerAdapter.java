package com.ebiz.msoclistar;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.security.oauth2.resource.ResourceServerProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import org.springframework.http.HttpMethod;

import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.token.grant.client.ClientCredentialsResourceDetails;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.authentication.BearerTokenExtractor;

import org.springframework.security.oauth2.provider.authentication.TokenExtractor;
import org.springframework.security.oauth2.provider.token.AccessTokenConverter;
import org.springframework.security.oauth2.provider.token.DefaultAccessTokenConverter;

import org.springframework.security.oauth2.provider.token.RemoteTokenServices;
import org.springframework.security.oauth2.provider.token.ResourceServerTokenServices;
import org.springframework.security.web.authentication.preauth.AbstractPreAuthenticatedProcessingFilter;
import org.springframework.web.filter.OncePerRequestFilter;

import com.ebiz.common.security.resource.service.CustomUserInfoTokenService;

//import com.ebiz.msclienttest.service.impl.CustomUserInfoTokenService;

@Configuration
public class ResourceServerAdapter extends ResourceServerConfigurerAdapter {

	private TokenExtractor tokenExtractor = new BearerTokenExtractor();
	
	private final Logger logger = LoggerFactory.getLogger(ResourceServerAdapter.class);
	
	

	public void configure(HttpSecurity http) throws Exception {
		

		
		http.addFilterAfter(new OncePerRequestFilter() {
			@Override
			protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response,
					FilterChain filterChain) throws ServletException, IOException {
				if (tokenExtractor.extract(request) == null) {
					SecurityContextHolder.clearContext();
				}
				filterChain.doFilter(request, response);
			}
		}, AbstractPreAuthenticatedProcessingFilter.class);
		http.csrf().disable();

		http
				.authorizeRequests().antMatchers(HttpMethod.GET, "/api/**").access("#oauth2.hasScope('read')")
				.antMatchers(HttpMethod.POST, "/api/**").access("#oauth2.hasScope('write')")
				.antMatchers(HttpMethod.PATCH, "/api/**").access("#oauth2.hasScope('write')")
				.antMatchers(HttpMethod.PUT, "/api/**").access("#oauth2.hasScope('write')")
				.antMatchers(HttpMethod.DELETE, "/api/**").access("#oauth2.hasScope('write')");

	}

	@Bean
	public AccessTokenConverter accessTokenConverter() {
		return new DefaultAccessTokenConverter();
	}
	
	@Value("${security.oauth2.client.client-id}")
    private String clientId;
	
	@Value("${security.oauth2.client.id}")
    private String resourceId;
	
	@Value("${security.oauth2.client.client-secret}")
    private String clientSecret;
	
	@Value("${security.oauth2.client.access-token-uri}")
    private String accessTokenUri;
	
	@Override
	public void configure(ResourceServerSecurityConfigurer resources) {
		RemoteTokenServices tokenService = new RemoteTokenServices();
		
		logger.info("clientId:{}",clientId);
		logger.info("resourceId:{}",resourceId);
		logger.info("clientSecret:{}",clientSecret);
		logger.info("accessTokenUri:{}",accessTokenUri);
		
		tokenService.setClientId(clientId);
		tokenService.setClientSecret(clientSecret);
		tokenService.setCheckTokenEndpointUrl(accessTokenUri);
		tokenService.setAccessTokenConverter(accessTokenConverter());
		resources.resourceId(resourceId).tokenServices(tokenService);
		   
	    	resources.tokenServices(tokenServices());
		
	}
	
	@Bean
	@ConfigurationProperties(prefix = "security.oauth2.client")
	public ClientCredentialsResourceDetails clientCredentialsResourceDetails() {
		return new ClientCredentialsResourceDetails();
	}
	
	@Bean
	public OAuth2RestTemplate clientCredentialsRestTemplate() {
		return new OAuth2RestTemplate(clientCredentialsResourceDetails());
	}

	
	@Autowired
	private ResourceServerProperties sso;
	
	@Bean
	public ResourceServerTokenServices tokenServices() {
	    return new CustomUserInfoTokenService(sso.getUserInfoUri(), sso.getClientId());
	}
	

}