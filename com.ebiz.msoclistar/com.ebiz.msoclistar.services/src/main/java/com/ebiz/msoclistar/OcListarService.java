package com.ebiz.msoclistar;

import java.sql.Date;
//import java.sql.Timestamp;
import java.util.List;

import com.ebiz.msoclistar.Oc;

public interface OcListarService {

    List<Oc> list(String ruc_proveedor, String razonsocial_proveedor, String ruc_comprador,
            String razonsocial_comprador, String nro_rfq, String nro_oc, String estado, Date fechainicio,
            Date fechafin, List<String> tipo_oc, String org_id, String order_columns, String org_idcomprador);
    
    ResponseListarOc datatableList(RequestListarOc requestOc);
    
}
