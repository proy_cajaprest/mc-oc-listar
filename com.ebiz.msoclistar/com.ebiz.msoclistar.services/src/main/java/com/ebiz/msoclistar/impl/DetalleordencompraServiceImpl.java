package com.ebiz.msoclistar.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ebiz.msoclistar.Ordencompra;
import com.ebiz.msoclistar.ProductoxOC;
import com.ebiz.msoclistar.RequestDetOC;
import com.ebiz.msoclistar.ResponseDetOC;
import com.ebiz.msoclistar.DetalleordencompraRepository;
import com.ebiz.msoclistar.DetalleordencompraService;

@Service
public class DetalleordencompraServiceImpl implements DetalleordencompraService{
	
	@Autowired
	private DetalleordencompraRepository detalleordencompraRepository;
	
	@Override
	public ResponseDetOC data(RequestDetOC request) {
		detalleordencompraRepository.setPerfil(request.getPerfil());
		detalleordencompraRepository.setOrigen(request.getOrigen());
		detalleordencompraRepository.setCabecera(request.getCabecera());
		detalleordencompraRepository.setSolo_productos(request.getSolo_productos());
		
		int flag = request.getCabecera();
		ResponseDetOC responseOC = new ResponseDetOC();
		if (flag != 0) {
			Ordencompra result = detalleordencompraRepository.detalleOrdenCompra(request.getId());
			responseOC.setData(result);
		}else if (flag == 0) {
			List<ProductoxOC> result_productos = detalleordencompraRepository.detalleProductosOC(request.getId());
			responseOC.setData_productos(result_productos);
		}
		
		return responseOC;
		
	}
}
