package com.ebiz.msoclistar.impl;

import java.sql.Date;
//import java.sql.Timestamp;
import java.util.ArrayList;
//import java.util.Collections;
//import java.util.Comparator;
import java.util.List;

//import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ebiz.msoclistar.Oc;
import com.ebiz.msoclistar.OcListarRepository;
import com.ebiz.msoclistar.OcListarService;
import com.ebiz.msoclistar.RequestListarOc;
import com.ebiz.msoclistar.ResponseListarOc;

@Service
public class OcListarServiceImpl implements OcListarService {

	@Autowired
	private OcListarRepository ocRepository;

	public List<Oc> list(String ruc_proveedor, String razonsocial_proveedor, String ruc_comprador,
			String razonsocial_comprador, String nro_rfq, String nro_oc, String estado, Date fechainicio,
			Date fechafin, List<String> tipo_oc, String org_id, String order_columns, String org_idcomprador) {
		return ocRepository.list(ruc_proveedor, razonsocial_proveedor, ruc_comprador, razonsocial_comprador, nro_rfq,
				nro_oc, estado, fechainicio, fechafin, tipo_oc, org_id, order_columns, org_idcomprador);
	}

	@Override
	public ResponseListarOc datatableList(RequestListarOc request) {
		ocRepository.setPerfil(request.getPerfil());
		ocRepository.setOrigen(request.getOrigen());
		ocRepository.setLista_columnas(request.getColumn_names());
		List<Oc> resultlist = ocRepository.list(request.getVc_rucproveedor(), request.getVc_razonsocialproveedor(),
				request.getVc_ruccomprador(), request.getVc_razonsocialcomprador(), request.getVc_numeroseguimiento_rfq(),
				request.getVc_numeroseguimiento_oc(), request.getVc_estado(), request.getTs_fecharegistro_inicio(), request.getTs_fecharegistro_fin(),
				request.getVc_tipooc(), request.getOrg_id(), request.getOrder_columns(), request.getOrg_idcomprador());///falta revisar lo que sigue
		List<Oc> returnlist = new ArrayList<Oc>();
		ResponseListarOc responseOc = new ResponseListarOc();
		responseOc.setRecordsTotal(resultlist.size());
		responseOc.setRecordsFiltered(resultlist.size());
		if (request.getLength() < 0) {
			if (request.getStart() == 0) {
				responseOc.setData(resultlist);
				responseOc.setDraw(request.getDraw());
				return responseOc;
			}
			request.setLength(resultlist.size());
		}
		int i;
		for (i = request.getStart(); i < resultlist.size(); i++) {
			if (i - request.getStart() >= request.getLength())
				break;
			returnlist.add(resultlist.get(i));
		}
		responseOc.setDraw(request.getDraw());
		responseOc.setData(returnlist);
		return responseOc;
	}
	
	/*
	public List<Oc> ordenarListaOc(List<Oc> data, List<String> data_columns,List<Integer> order_columns, List<String> order_direc){
		
		Collections.sort(data, new ComparadorOcs((String[])data_columns.toArray(), (Integer[])order_columns.toArray(), (String[])order_direc.toArray()));
		
		return null;
	}
	*/
}

/*
class ComparadorOcs implements Comparator<Oc>{
	
	private String[] data_columns;
	private Integer[] order_columns;
	private String[] order_direc;

	public ComparadorOcs(String[] data_columns, Integer[] order_columns, String[] order_direc) {
		this.data_columns = data_columns;
		this.order_columns = order_columns;
		this.order_direc = order_direc;
	}

	@Override
	public int compare(Oc arg0, Oc arg1) {
		int index;
		String criterio = "", order = "";
		for (int i=0; i<order_columns.length; i++) {
			index = order_columns[i];
			criterio = data_columns[index];
			order = order_direc[i];
			
			int comparacion = 0;
			switch(criterio) {
			case "in_idoc":
        		comparacion = arg0.getId_oc().compareTo(arg1.getId_oc());
        		break;
        	case "nro_oc":
        		comparacion = arg0.getNro_oc().compareTo(arg1.getNro_oc());
        		break;
        	case "ch_estado":
        		comparacion = arg0.getEstado().compareTo(arg1.getEstado());
        		break;
        	case "vc_tipooc":
        		comparacion = arg0.getTipo_oc().compareTo(arg1.getTipo_oc());
        		break;
        	case "in_idusuariocomprador":
        		comparacion = arg0.getId_usuariocomprador().compareTo(arg1.getId_usuariocomprador());
        		break;
        	case "vc_nombreusuariocomprador":
        		comparacion = arg0.getNombre_usuariocomprador().compareTo(arg1.getNombre_usuariocomprador());
        		break;
        	case "in_idusuarioproveedor":
        		comparacion = arg0.getId_usuarioproveedor().compareTo(arg1.getId_usuarioproveedor());
        		break;
        	case "vc_nombreusuarioproveedor":
        		comparacion = arg0.getNombre_usuarioproveedor().compareTo(arg1.getNombre_usuarioproveedor());
        		break;
        	case "vc_atenciona":
        		comparacion = arg0.getAtenciona().compareTo(arg1.getAtenciona());
        		break;
        	case "in_idorganizacioncompradora":
        		comparacion = arg0.getId_organizacioncompradora().compareTo(arg1.getId_organizacioncompradora());
        		break;
        	case "vc_razonsocialcomprador":
        		comparacion = arg0.getRazonsocial_comprador().compareTo(arg1.getRazonsocial_comprador());
        		break;
        	case "in_idorganizacionproveedora":
        		comparacion = arg0.getId_organizacionproveedora().compareTo(arg1.getId_organizacionproveedora());
        		break;
        	case "vc_razonsocialproveedor":
        		comparacion = arg0.getRazonsocial_proveedor().compareTo(arg1.getRazonsocial_proveedor());
        		break;
        	case "fl_valortotal":
        		comparacion = arg0.getValortotal().compareTo(arg1.getValortotal());
        		break;
        	case "vc_moneda":
        		comparacion = arg0.getMoneda().compareTo(arg1.getMoneda());
        		break;
        	case "ts_fechacreacion":
        		comparacion = arg0.getFechacreacion().compareTo(arg1.getFechacreacion());
        		break;
			}
			
			if(comparacion != 0) {
				return order.equals("asc") ? comparacion : -comparacion;
			}
		}
		return 0;
	}
	
}
*/