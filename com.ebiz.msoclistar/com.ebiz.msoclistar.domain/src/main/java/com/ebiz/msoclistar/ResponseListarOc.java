package com.ebiz.msoclistar;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

import com.ebiz.msoclistar.Oc;

public class ResponseListarOc {
	
	@JsonProperty("statuscode")
	private String statuscode = "0000";
	
	@JsonProperty("message")
	private String message = "Accion efectuada con exito";
	
	@JsonProperty("draw")
	private int draw;
	
	@JsonProperty("recordsTotal")
	private int recordsTotal;
	
	@JsonProperty("recordsFiltered")
	private int recordsFiltered = 0;
	
	@JsonProperty("data")
	private List<Oc> data;
	
	//
	
	public String getStatuscode() {
		return statuscode;
	}

	public void setStatuscode(String statuscode) {
		this.statuscode = statuscode;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public int getDraw() {
		return draw;
	}

	public void setDraw(int draw) {
		this.draw = draw;
	}

	public int getRecordsTotal() {
		return recordsTotal;
	}

	public void setRecordsTotal(int recordsTotal) {
		this.recordsTotal = recordsTotal;
	}

	public int getRecordsFiltered() {
		return recordsFiltered;
	}

	public void setRecordsFiltered(int recordsFiltered) {
		this.recordsFiltered = recordsFiltered;
	}

	public List<Oc> getData() {
		return data;
	}

	public void setData(List<Oc> data) {
		this.data = data;
	}
	
}
