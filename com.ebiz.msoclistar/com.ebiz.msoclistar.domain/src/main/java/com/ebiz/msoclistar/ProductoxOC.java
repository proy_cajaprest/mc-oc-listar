package com.ebiz.msoclistar;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;

import java.util.List;

public class ProductoxOC {

	@JsonProperty("AtributosProducto")
	private List<AtributoxProductoxOC> lista_atributoxprod;
	
	@JsonProperty("SubItemsProducto")
	private List<SubitemxProductoxOC> lista_subitemxprod;

	@JsonProperty("IdProductoxOc")
	private String in_idproductoxoc;
	
	@JsonProperty("IdProducto")
	private String in_idproducto;
	
	@JsonProperty("CodigoProducto")
	private String vc_codigoproductoorg;
	
	@JsonProperty("TipoProducto")
	private Integer in_tipoproducto;
	
	@JsonProperty("DescripcionProducto")
	private String vc_descortapro;
	
	@JsonProperty("CodigoUsuarioCreacion")
	private String in_codigousuariocreacion;
	
	@JsonProperty("Habilitado")
	private Integer in_habilitado;
	
	@JsonProperty("EstadoProducto")
	private String vc_estadoprod;
	
	@JsonProperty("IdOrganizacionComprador")
	private String in_idorganizacion;
	
	@JsonProperty("PosicionProducto")
	private Integer in_posicion;
	
	@JsonProperty("FechaEntregaProducto")
	private String ts_fechaentregaproducto;
	
	@JsonProperty("PrecioUnitarioProducto")
	private BigDecimal fl_precioproducto;
	
	@JsonProperty("CantidadProducto")
	private BigDecimal fl_cantidadproducto;
	
	@JsonProperty("CantidadDespachada")
	private BigDecimal fl_cantidaddespachada;
	
	@JsonProperty("PrecioProducto")
	private BigDecimal fl_preciototalproducto;
	
	@JsonProperty("IdUnidadProducto")
	private Integer in_idunidadproducto;
	
	//@JsonProperty("Posicion")
	//private String vc_posicion;	
	
	@JsonProperty("IdOc")
	private String in_idoc;
	
	@JsonProperty("ProductoCat")
	private Integer in_idproductocat;
	
	@JsonProperty("IdCategoria")
	private Integer in_idcategoria;
	
	@JsonProperty("DescripcionLarga")
	private String vc_deslargapro;
	
	@JsonProperty("Campo1")
	private String vc_campo1;
	
	@JsonProperty("Campo2")
	private String vc_campo2;
	
	@JsonProperty("Campo3")
	private String vc_campo3;
	
	@JsonProperty("Campo4")
	private String vc_campo4;
	
	@JsonProperty("Campo5")
	private String vc_campo5;
	
	@JsonProperty("IdUsuariomodificacion")
	private String in_idusuariomodificacion;
	
	@JsonProperty("FechaCreacion")
	private String ts_fechahoracreacion;
	
	@JsonProperty("FechaModificacion")
	private String ts_fechahoramodificacion;
	
	@JsonProperty("ItemRequisicion")
	private Integer in_itemrequisicion;
	
	@JsonProperty("CantidadProductoPend")
	private BigDecimal fl_cantidadproductopend;
	
	@JsonProperty("NumeroParte")
	private String vc_numeroparte;
	
	@JsonProperty("IdProdB2M")
	private Integer in_idprodb2m;
	
	//@JsonProperty("ItemRequisicion")
	//private String vc_itemrequisicion;
	
	//@JsonProperty("vc_partnumber")
	//private String vc_partnumber;
	
	@JsonProperty("IdRequerimiento")
	private Integer in_idrequerimiento;
	
	@JsonProperty("PosicionReq")
	private Integer in_posicionreq;
	
	@JsonProperty("vc_commodity")
	private String vc_commodity;
	
	@JsonProperty("CantidadRecepcionada")
	private BigDecimal fl_cantidadrecepcionada;
	
	@JsonProperty("UnidadProducto")
	private String vc_unidadproducto;
	
	@JsonProperty("Impuestos")
	private BigDecimal de_impuestos;
	
	@JsonProperty("IdTablaunidadMedida")
	private String vc_idtablaunidad;
	
	@JsonProperty("IdRegistroUnidadMedida")
	private String vc_idregistrounidad;

	public List<AtributoxProductoxOC> getLista_atributoxprod() {
		return lista_atributoxprod;
	}

	public void setLista_atributoxprod(List<AtributoxProductoxOC> lista_atributoxprod) {
		this.lista_atributoxprod = lista_atributoxprod;
	}

	public List<SubitemxProductoxOC> getLista_subitemxprod() {
		return lista_subitemxprod;
	}

	public void setLista_subitemxprod(List<SubitemxProductoxOC> lista_subitemxprod) {
		this.lista_subitemxprod = lista_subitemxprod;
	}

	public String getIn_idproductoxoc() {
		return in_idproductoxoc;
	}

	public void setIn_idproductoxoc(String in_idproductoxoc) {
		this.in_idproductoxoc = in_idproductoxoc;
	}

	public String getIn_idproducto() {
		return in_idproducto;
	}

	public void setIn_idproducto(String in_idproducto) {
		this.in_idproducto = in_idproducto;
	}

	public String getVc_codigoproductoorg() {
		return vc_codigoproductoorg;
	}

	public void setVc_codigoproductoorg(String vc_codigoproductoorg) {
		this.vc_codigoproductoorg = vc_codigoproductoorg;
	}

	public Integer getIn_tipoproducto() {
		return in_tipoproducto;
	}

	public void setIn_tipoproducto(Integer in_tipoproducto) {
		this.in_tipoproducto = in_tipoproducto;
	}

	public String getVc_descortapro() {
		return vc_descortapro;
	}

	public void setVc_descortapro(String vc_descortapro) {
		this.vc_descortapro = vc_descortapro;
	}

	public String getIn_codigousuariocreacion() {
		return in_codigousuariocreacion;
	}

	public void setIn_codigousuariocreacion(String in_codigousuariocreacion) {
		this.in_codigousuariocreacion = in_codigousuariocreacion;
	}

	public Integer getIn_habilitado() {
		return in_habilitado;
	}

	public void setIn_habilitado(Integer in_habilitado) {
		this.in_habilitado = in_habilitado;
	}

	public String getVc_estadoprod() {
		return vc_estadoprod;
	}

	public void setVc_estadoprod(String vc_estadoprod) {
		this.vc_estadoprod = vc_estadoprod;
	}

	public String getIn_idorganizacion() {
		return in_idorganizacion;
	}

	public void setIn_idorganizacion(String in_idorganizacion) {
		this.in_idorganizacion = in_idorganizacion;
	}

	public Integer getIn_posicion() {
		return in_posicion;
	}

	public void setIn_posicion(Integer in_posicion) {
		this.in_posicion = in_posicion;
	}

	public String getTs_fechaentregaproducto() {
		return ts_fechaentregaproducto;
	}

	public void setTs_fechaentregaproducto(String ts_fechaentregaproducto) {
		this.ts_fechaentregaproducto = ts_fechaentregaproducto;
	}

	public BigDecimal getFl_precioproducto() {
		return fl_precioproducto;
	}

	public void setFl_precioproducto(BigDecimal fl_precioproducto) {
		this.fl_precioproducto = fl_precioproducto;
	}

	public BigDecimal getFl_cantidadproducto() {
		return fl_cantidadproducto;
	}

	public void setFl_cantidadproducto(BigDecimal fl_cantidadproducto) {
		this.fl_cantidadproducto = fl_cantidadproducto;
	}

	public BigDecimal getFl_preciototalproducto() {
		return fl_preciototalproducto;
	}

	public void setFl_preciototalproducto(BigDecimal fl_preciototalproducto) {
		this.fl_preciototalproducto = fl_preciototalproducto;
	}

	public Integer getIn_idunidadproducto() {
		return in_idunidadproducto;
	}

	public void setIn_idunidadproducto(Integer in_idunidadproducto) {
		this.in_idunidadproducto = in_idunidadproducto;
	}

	public String getIn_idoc() {
		return in_idoc;
	}

	public void setIn_idoc(String in_idoc) {
		this.in_idoc = in_idoc;
	}

	public Integer getIn_idproductocat() {
		return in_idproductocat;
	}

	public void setIn_idproductocat(Integer in_idproductocat) {
		this.in_idproductocat = in_idproductocat;
	}

	public Integer getIn_idcategoria() {
		return in_idcategoria;
	}

	public void setIn_idcategoria(Integer in_idcategoria) {
		this.in_idcategoria = in_idcategoria;
	}

	public String getVc_deslargapro() {
		return vc_deslargapro;
	}

	public void setVc_deslargapro(String vc_deslargapro) {
		this.vc_deslargapro = vc_deslargapro;
	}

	public String getVc_campo1() {
		return vc_campo1;
	}

	public void setVc_campo1(String vc_campo1) {
		this.vc_campo1 = vc_campo1;
	}

	public String getVc_campo2() {
		return vc_campo2;
	}

	public void setVc_campo2(String vc_campo2) {
		this.vc_campo2 = vc_campo2;
	}

	public String getVc_campo3() {
		return vc_campo3;
	}

	public void setVc_campo3(String vc_campo3) {
		this.vc_campo3 = vc_campo3;
	}

	public String getVc_campo4() {
		return vc_campo4;
	}

	public void setVc_campo4(String vc_campo4) {
		this.vc_campo4 = vc_campo4;
	}

	public String getVc_campo5() {
		return vc_campo5;
	}

	public void setVc_campo5(String vc_campo5) {
		this.vc_campo5 = vc_campo5;
	}

	public String getIn_idusuariomodificacion() {
		return in_idusuariomodificacion;
	}

	public void setIn_idusuariomodificacion(String in_idusuariomodificacion) {
		this.in_idusuariomodificacion = in_idusuariomodificacion;
	}

	public String getTs_fechahoracreacion() {
		return ts_fechahoracreacion;
	}

	public void setTs_fechahoracreacion(String ts_fechahoracreacion) {
		this.ts_fechahoracreacion = ts_fechahoracreacion;
	}

	public String getTs_fechahoramodificacion() {
		return ts_fechahoramodificacion;
	}

	public void setTs_fechahoramodificacion(String ts_fechahoramodificacion) {
		this.ts_fechahoramodificacion = ts_fechahoramodificacion;
	}

	public Integer getIn_itemrequisicion() {
		return in_itemrequisicion;
	}

	public void setIn_itemrequisicion(Integer in_itemrequisicion) {
		this.in_itemrequisicion = in_itemrequisicion;
	}

	public BigDecimal getFl_cantidadproductopend() {
		return fl_cantidadproductopend;
	}

	public void setFl_cantidadproductopend(BigDecimal fl_cantidadproductopend) {
		this.fl_cantidadproductopend = fl_cantidadproductopend;
	}

	public String getVc_numeroparte() {
		return vc_numeroparte;
	}

	public void setVc_numeroparte(String vc_numeroparte) {
		this.vc_numeroparte = vc_numeroparte;
	}

	public Integer getIn_idprodb2m() {
		return in_idprodb2m;
	}

	public void setIn_idprodb2m(Integer in_idprodb2m) {
		this.in_idprodb2m = in_idprodb2m;
	}

	public Integer getIn_idrequerimiento() {
		return in_idrequerimiento;
	}

	public void setIn_idrequerimiento(Integer in_idrequerimiento) {
		this.in_idrequerimiento = in_idrequerimiento;
	}

	public Integer getIn_posicionreq() {
		return in_posicionreq;
	}

	public void setIn_posicionreq(Integer in_posicionreq) {
		this.in_posicionreq = in_posicionreq;
	}

	public String getVc_commodity() {
		return vc_commodity;
	}

	public void setVc_commodity(String vc_commodity) {
		this.vc_commodity = vc_commodity;
	}

	public BigDecimal getFl_cantidadrecepcionada() {
		return fl_cantidadrecepcionada;
	}

	public void setFl_cantidadrecepcionada(BigDecimal fl_cantidadrecepcionada) {
		this.fl_cantidadrecepcionada = fl_cantidadrecepcionada;
	}

	public String getVc_unidadproducto() {
		return vc_unidadproducto;
	}

	public void setVc_unidadproducto(String vc_unidadproducto) {
		this.vc_unidadproducto = vc_unidadproducto;
	}

	public BigDecimal getDe_impuestos() {
		return de_impuestos;
	}

	public void setDe_impuestos(BigDecimal de_impuestos) {
		this.de_impuestos = de_impuestos;
	}

	public String getVc_idtablaunidad() {
		return vc_idtablaunidad;
	}

	public void setVc_idtablaunidad(String vc_idtablaunidad) {
		this.vc_idtablaunidad = vc_idtablaunidad;
	}

	public String getVc_idregistrounidad() {
		return vc_idregistrounidad;
	}

	public void setVc_idregistrounidad(String vc_idregistrounidad) {
		this.vc_idregistrounidad = vc_idregistrounidad;
	}

	public BigDecimal getFl_cantidaddespachada() {
		return fl_cantidaddespachada;
	}

	public void setFl_cantidaddespachada(BigDecimal fl_cantidaddespachada) {
		this.fl_cantidaddespachada = fl_cantidaddespachada;
	}

	
}
