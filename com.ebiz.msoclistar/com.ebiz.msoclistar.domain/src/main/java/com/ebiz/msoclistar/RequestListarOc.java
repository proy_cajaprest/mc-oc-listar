package com.ebiz.msoclistar;

import java.sql.Date;
//import java.sql.Timestamp;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

public class RequestListarOc {
	
	private int draw;

	private int start;
	
	private int length;
	
	private char perfil;
	
	private int origen;					// 0: PEESEACE	1: PEB2M
	
	//ruc_proveedor
	private String vc_rucproveedor;
	
	//razonsocial_proveedor
	private String vc_razonsocialproveedor;
	
	private String vc_ruccomprador;
	
	private String vc_razonsocialcomprador;
	
	// nro requerimiento
	private String vc_numeroseguimiento_rfq;
	
	//nro oc
	private String vc_numeroseguimiento_oc;
	
	// estado
	private String vc_estado;
	
	// fecha_creacion_inicio
	private Date ts_fecharegistro_inicio;
	
	// fecha_creacion_fin
	private Date ts_fecharegistro_fin;
	
	//tipo oc
	private List<String> vc_tipooc;
	
	// ADICIONALES

	private List<String> column_names;

	private String order_columns;

	private String org_id;
	
	private String org_idcomprador;
	
	/////////////
	
	public int getDraw() {
		return draw;
	}

	public int getOrigen() {
		return origen;
	}

	public void setOrigen(int origen) {
		this.origen = origen;
	}

	public char getPerfil() {
		return perfil;
	}

	public void setPerfil(char perfil) {
		this.perfil = perfil;
	}

	public List<String> getVc_tipooc() {
		return vc_tipooc;
	}

	public void setVc_tipooc(List<String> vc_tipooc) {
		this.vc_tipooc = vc_tipooc;
	}

	public void setDraw(int draw) {
		this.draw = draw;
	}

	public int getStart() {
		return start;
	}

	public void setStart(int start) {
		this.start = start;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public String getVc_rucproveedor() {
		return vc_rucproveedor;
	}

	public void setVc_rucproveedor(String vc_rucproveedor) {
		this.vc_rucproveedor = vc_rucproveedor;
	}

	public String getVc_razonsocialproveedor() {
		return vc_razonsocialproveedor;
	}

	public void setVc_razonsocialproveedor(String vc_razonsocialproveedor) {
		this.vc_razonsocialproveedor = vc_razonsocialproveedor;
	}

	public String getVc_ruccomprador() {
		return vc_ruccomprador;
	}

	public void setVc_ruccomprador(String vc_ruccomprador) {
		this.vc_ruccomprador = vc_ruccomprador;
	}

	public String getVc_razonsocialcomprador() {
		return vc_razonsocialcomprador;
	}

	public void setVc_razonsocialcomprador(String vc_razonsocialcomprador) {
		this.vc_razonsocialcomprador = vc_razonsocialcomprador;
	}

	public String getVc_numeroseguimiento_rfq() {
		return vc_numeroseguimiento_rfq;
	}

	public void setVc_numeroseguimiento_rfq(String vc_numeroseguimiento_rfq) {
		this.vc_numeroseguimiento_rfq = vc_numeroseguimiento_rfq;
	}

	public String getVc_numeroseguimiento_oc() {
		return vc_numeroseguimiento_oc;
	}

	public void setVc_numeroseguimiento_oc(String vc_numeroseguimiento_oc) {
		this.vc_numeroseguimiento_oc = vc_numeroseguimiento_oc;
	}

	public String getVc_estado() {
		return vc_estado;
	}

	public void setVc_estado(String vc_estado) {
		this.vc_estado = vc_estado;
	}

	public Date getTs_fecharegistro_inicio() {
		return ts_fecharegistro_inicio;
	}

	public void setTs_fecharegistro_inicio(Date ts_fecharegistro_inicio) {
		this.ts_fecharegistro_inicio = ts_fecharegistro_inicio;
	}

	public Date getTs_fecharegistro_fin() {
		return ts_fecharegistro_fin;
	}

	public void setTs_fecharegistro_fin(Date ts_fecharegistro_fin) {
		this.ts_fecharegistro_fin = ts_fecharegistro_fin;
	}

	public List<String> getColumn_names() {
		return column_names;
	}

	public void setColumn_names(List<String> column_names) {
		this.column_names = column_names;
	}

	public String getOrder_columns() {
		return this.order_columns;
	}	

	public void setOrder_columns(Map<String, String> params, String perfil) {
		Map<String, String> map = new HashMap<String, String>();
		map.put("CodigoOrden", "in_idoc");
		map.put("NumeroOrden", "ch_numeroseguimiento");
		if(perfil.equals("C")) {
          map.put("EstadoOrden", "vc_estado_comprador");
        }else if(perfil.equals("P")) {        	
          map.put("EstadoOrden", "vc_estado_proveedor");
		}
		map.put("TipoOrden", "vc_tipooc");
		map.put("AtencionA", "vc_atenciona");
		map.put("NITComprador", "vc_ruccomprador");
		map.put("UsuarioComprador", "vc_nombreusuariocomprador");
		map.put("NombreComprador", "vc_razonsocialcomprador");
		map.put("NITVendedor", "vc_rucproveedor");
		map.put("UsuarioProveedor", "vc_nombreusuarioproveedor");
		map.put("NombreVendedor", "vc_razonsocialproveedor");
		map.put("ValorTotal", "fl_valortotal");
		map.put("MonedaOrden", "vc_moneda");
		map.put("Fecha", "ts_fechacreacion");
		map.put("NumeroRfq", "ch_numerorfq");
		map.put("Version", "in_version");
		map.put("FechaRegistro", "ts_fecharegistro");
	
		this.order_columns = "";  String column = null, pre = null, dir = null;  int i = 0;  Boolean f=true;
        while (true) {
			dir=params.get("order["+i+"][dir]");
			if(dir!=null) {
				column=map.get(params.get("columns["+params.get("order["+i+"][column]")+"][name]"));
				if(column!=null) {
					if(f){pre=" order by "; f=false;}else{pre=", ";};
					this.order_columns += pre+column+" "+dir;
				}
			}else { break; } i++;
        }
	}
	
	public String getOrg_id() {
		return org_id;
	}

	public void setOrg_id(String org_id) {
		this.org_id = org_id;
	}

	public String getOrg_idcomprador() {
		return org_idcomprador;
	}

	public void setOrg_idcomprador(String org_idcomprador) {
		this.org_idcomprador = org_idcomprador;
	}
		
}