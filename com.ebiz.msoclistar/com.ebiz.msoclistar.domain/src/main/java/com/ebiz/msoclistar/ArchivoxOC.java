package com.ebiz.msoclistar;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ArchivoxOC {
	
	@JsonProperty("IdArchivoOrden")
	private String in_idarchivooc;

	@JsonProperty("NombreArchivo")
	private String vc_nombre;

	@JsonProperty("ContenidoArchivo")
	private String vc_ruta;
	
	@JsonProperty("DescripcionArchivo")
	private String vc_descripcion;
	
	@JsonProperty("CodigoUsuarioCreacion")
	private String in_codigousuariocreacion;
	
	@JsonProperty("CodigoUsuarioModificacion")
	private String in_codigousuariomodificacion ;
	
	@JsonProperty("Habilitado")
	private Integer in_habilitado;

	public String getIn_idarchivooc() {
		return in_idarchivooc;
	}

	public void setIn_idarchivooc(String in_idarchivooc) {
		this.in_idarchivooc = in_idarchivooc;
	}
	
	public String getVc_nombre() {
		return vc_nombre;
	}

	public void setVc_nombre(String vc_nombre) {
		this.vc_nombre = vc_nombre;
	}

	public String getVc_ruta() {
		return vc_ruta;
	}

	public void setVc_ruta(String vc_ruta) {
		this.vc_ruta = vc_ruta;
	}

	public String getVc_descripcion() {
		return vc_descripcion;
	}

	public void setVc_descripcion(String vc_descripcion) {
		this.vc_descripcion = vc_descripcion;
	}

	public String getIn_codigousuariocreacion() {
		return in_codigousuariocreacion;
	}

	public void setIn_codigousuariocreacion(String in_codigousuariocreacion) {
		this.in_codigousuariocreacion = in_codigousuariocreacion;
	}

	public int getIn_habilitado() {
		return in_habilitado;
	}

	public void setIn_habilitado(int in_habilitado) {
		this.in_habilitado = in_habilitado;
	}

	public String getIn_codigousuariomodificacion() {
		return in_codigousuariomodificacion;
	}

	public void setIn_codigousuariomodificacion(String in_codigousuariomodificacion) {
		this.in_codigousuariomodificacion = in_codigousuariomodificacion;
	}

	public void setIn_habilitado(Integer in_habilitado) {
		this.in_habilitado = in_habilitado;
	}
}
