package com.ebiz.msoclistar;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AtributoxOC {
	
	@JsonProperty("NombreAtributoOrden")
	private String vc_nombreatributo;
	
	@JsonProperty("IdAtributoOrden")
	private String in_idatributo;
	
	@JsonProperty("ValorAtributoOrden")
	private String vc_valorenviado;

	@JsonProperty("AtributoOrdenObligatorio")
	private String vc_mandatorio;
	
	@JsonProperty("AtributoOrdenModificable")
	private String vc_modificable;
	
	@JsonProperty("UnidadAtributoOrden")
	private Integer in_idunidad ;
	
	@JsonProperty("CodigoUsuarioCreacion")
	private String in_codigousuariocreacion ;
	
	@JsonProperty("CodigoUsuarioModificacion")
	private String in_codigousuariomodificacion ;
	
	@JsonProperty("Habilitado")
	private Integer in_habilitado;

	public String getVc_nombreatributo() {
		return vc_nombreatributo;
	}

	public void setVc_nombreatributo(String vc_nombreatributo) {
		this.vc_nombreatributo = vc_nombreatributo;
	}

	public String getIn_idatributo() {
		return in_idatributo;
	}

	public void setIn_idatributo(String in_idatributo) {
		this.in_idatributo = in_idatributo;
	}

	public String getVc_valorenviado() {
		return vc_valorenviado;
	}

	public void setVc_valorenviado(String vc_valorenviado) {
		this.vc_valorenviado = vc_valorenviado;
	}

	public String getVc_mandatorio() {
		return vc_mandatorio;
	}

	public void setVc_mandatorio(String vc_mandatorio) {
		this.vc_mandatorio = vc_mandatorio;
	}

	public String getVc_modificable() {
		return vc_modificable;
	}

	public void setVc_modificable(String vc_modificable) {
		this.vc_modificable = vc_modificable;
	}

	public Integer getIn_idunidad() {
		return in_idunidad;
	}

	public void setIn_idunidad(Integer in_idunidad) {
		this.in_idunidad = in_idunidad;
	}

	public Integer getIn_habilitado() {
		return in_habilitado;
	}

	public void setIn_habilitado(Integer in_habilitado) {
		this.in_habilitado = in_habilitado;
	}

	public String getIn_codigousuariocreacion() {
		return in_codigousuariocreacion;
	}

	public void setIn_codigousuariocreacion(String in_codigousuariocreacion) {
		this.in_codigousuariocreacion = in_codigousuariocreacion;
	}

	public String getIn_codigousuariomodificacion() {
		return in_codigousuariomodificacion;
	}

	public void setIn_codigousuariomodificacion(String in_codigousuariomodificacion) {
		this.in_codigousuariomodificacion = in_codigousuariomodificacion;
	}

	
	
}
