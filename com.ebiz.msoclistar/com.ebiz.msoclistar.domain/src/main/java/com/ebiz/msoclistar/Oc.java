package com.ebiz.msoclistar;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Oc {

    @JsonProperty("CodigoOrden")
    private String in_idoc;
    
    @JsonProperty("NumeroOrden")
    private String vc_numeroseguimiento_oc;

    @JsonProperty("EstadoOrden")
    private String vc_estado;
    
    @JsonProperty("TipoOrden")
	private String vc_tipooc;
    
    @JsonProperty("Version")
    private Integer in_version;
    
    @JsonProperty("CodigoUsuarioComprador")
    private String id_usuariocomprador;

    @JsonProperty("UsuarioComprador")
    private String vc_nombre_usrcomprador;
    
    @JsonProperty("CodigoUsuarioProveedor")
    private String id_usuarioproveedor;
    
    @JsonProperty("UsuarioProveedor")
    private String vc_nombre_usrproveedor;
    
    //@JsonProperty("RazonSocialProveedor")
    //private String vc_nombre_orgproveedor;

    @JsonProperty("AtencionA")
    private String vc_atenciona;
    
    @JsonProperty("CodigoOrgComprador")
    private String id_organizacioncompradora;

    @JsonProperty("NITComprador")
	private String vc_ruccomprador;

    @JsonProperty("NombreComprador")
    private String vc_razonsocialcomprador;

    @JsonProperty("CodigoOrgProveedor")
    private String id_organizacionproveedora;

	@JsonProperty("NITVendedor")
	private String vc_rucproveedor;
	
	@JsonProperty("NombreVendedor")
    private String vc_razonsocialproveedor;
	
    @JsonProperty("ValorTotal")
    private Double fl_valortotal;

    @JsonProperty("MonedaOrden")
	private String vc_moneda;

    @JsonProperty("Fecha")
	private String ts_fechacreacion;
    
    @JsonProperty("FechaRegistro")
    private String ts_fecharegistro;
	
	@JsonProperty("NumeroRfq")
    private String vc_numeroseguimiento_rfq;
	
	/////////////////////
	
	
	
	public String getVc_numeroseguimiento_oc() {
		return vc_numeroseguimiento_oc;
	}

	public String getVc_nombre_usrproveedor() {
		return vc_nombre_usrproveedor;
	}

	public void setVc_nombre_usrproveedor(String vc_nombre_usrproveedor) {
		this.vc_nombre_usrproveedor = vc_nombre_usrproveedor;
	}

	public String getVc_nombre_usrcomprador() {
		return vc_nombre_usrcomprador;
	}

	public void setVc_nombre_usrcomprador(String vc_nombre_usrcomprador) {
		this.vc_nombre_usrcomprador = vc_nombre_usrcomprador;
	}

	/*public String getVc_nombre_orgproveedor() {
		return vc_nombre_orgproveedor;
	}

	public void setVc_nombre_orgproveedor(String vc_nombre_orgproveedor) {
		this.vc_nombre_orgproveedor = vc_nombre_orgproveedor;
	}*/

	public String getVc_ruccomprador() {
		return vc_ruccomprador;
	}

	public void setVc_ruccomprador(String vc_ruccomprador) {
		this.vc_ruccomprador = vc_ruccomprador;
	}

	public String getVc_atenciona() {
		return vc_atenciona;
	}

	public void setVc_atenciona(String vc_atenciona) {
		this.vc_atenciona = vc_atenciona;
	}

	public String getTs_fecharegistro() {
		return ts_fecharegistro;
	}

	public void setTs_fecharegistro(String ts_fecharegistro) {
		this.ts_fecharegistro = ts_fecharegistro;
	}

	public Integer getIn_version() {
		return in_version;
	}

	public void setIn_version(Integer in_version) {
		this.in_version = in_version;
	}

	public String getIn_idoc() {
		return in_idoc;
	}

	public void setIn_idoc(String in_idoc) {
		this.in_idoc = in_idoc;
	}

	public void setVc_numeroseguimiento_oc(String vc_numeroseguimiento_oc) {
		this.vc_numeroseguimiento_oc = vc_numeroseguimiento_oc;
	}

	public String getVc_estado() {
		return vc_estado;
	}

	public void setVc_estado(String vc_estado) {
		this.vc_estado = vc_estado;
	}

	public String getVc_tipooc() {
		return vc_tipooc;
	}

	public void setVc_tipooc(String vc_tipooc) {
		this.vc_tipooc = vc_tipooc;
	}

	public String getId_usuariocomprador() {
		return id_usuariocomprador;
	}

	public void setId_usuariocomprador(String id_usuariocomprador) {
		this.id_usuariocomprador = id_usuariocomprador;
	}

	public String getId_usuarioproveedor() {
		return id_usuarioproveedor;
	}

	public void setId_usuarioproveedor(String id_usuarioproveedor) {
		this.id_usuarioproveedor = id_usuarioproveedor;
	}

	public String getId_organizacioncompradora() {
		return id_organizacioncompradora;
	}

	public void setId_organizacioncompradora(String id_organizacioncompradora) {
		this.id_organizacioncompradora = id_organizacioncompradora;
	}

	

	public String getVc_razonsocialcomprador() {
		return vc_razonsocialcomprador;
	}

	public void setVc_razonsocialcomprador(String vc_razonsocialcomprador) {
		this.vc_razonsocialcomprador = vc_razonsocialcomprador;
	}

	public String getId_organizacionproveedora() {
		return id_organizacionproveedora;
	}

	public void setId_organizacionproveedora(String id_organizacionproveedora) {
		this.id_organizacionproveedora = id_organizacionproveedora;
	}

	public String getVc_rucproveedor() {
		return vc_rucproveedor;
	}

	public void setVc_rucproveedor(String vc_rucproveedor) {
		this.vc_rucproveedor = vc_rucproveedor;
	}

	public String getVc_razonsocialproveedor() {
		return vc_razonsocialproveedor;
	}

	public void setVc_razonsocialproveedor(String vc_razonsocialproveedor) {
		this.vc_razonsocialproveedor = vc_razonsocialproveedor;
	}

	public Double getFl_valortotal() {
		return fl_valortotal;
	}

	public void setFl_valortotal(Double fl_valortotal) {
		this.fl_valortotal = fl_valortotal;
	}

	public String getVc_moneda() {
		return vc_moneda;
	}

	public void setVc_moneda(String vc_moneda) {
		this.vc_moneda = vc_moneda;
	}

	public String getTs_fechacreacion() {
		return ts_fechacreacion;
	}

	public void setTs_fechacreacion(String ts_fechacreacion) {
		this.ts_fechacreacion = ts_fechacreacion;
	}

	public String getVc_numeroseguimiento_rfq() {
		return vc_numeroseguimiento_rfq;
	}

	public void setVc_numeroseguimiento_rfq(String vc_numeroseguimiento_rfq) {
		this.vc_numeroseguimiento_rfq = vc_numeroseguimiento_rfq;
	}
	
}
