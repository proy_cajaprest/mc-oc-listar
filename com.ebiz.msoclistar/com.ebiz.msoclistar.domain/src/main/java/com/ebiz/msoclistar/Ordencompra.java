package com.ebiz.msoclistar;

import java.math.BigDecimal;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Ordencompra {

	@JsonProperty("AdjuntosOrden")
	private List<ArchivoxOC> lista_archivoxoc;

	@JsonProperty("AtributosOrden")
	private List<AtributoxOC> lista_atributoxoc;

	@JsonProperty("ProductosOrden")
	private List<ProductoxOC> lista_prodxoc;

	@JsonProperty("IdOc")
	private String idoc;

	@JsonProperty("CompradorUsuarioID")
	private String idusuariocomprador;

	@JsonProperty("IdUsuarioProveedor")
	private String idusuarioproveedor;

	@JsonProperty("UsuarioComprador") // nombre usuario comprador
	private String n_usuariocomprador;

	@JsonProperty("UsuarioProveedor")
	private String n_usuarioproveedor;

	@JsonProperty("NumeroOrden")
	private String numseguimiento;

	@JsonProperty("Version")
	private Integer version;

	@JsonProperty("idTipoOrden")
	private Integer tipo;

	@JsonProperty("PrioridadOrden")
	private String prioridad;

	@JsonProperty("IdQT")
	private String idqt;

	@JsonProperty("FechaRegistro")
	private String fecharegistro;

	@JsonProperty("LugarEntrega")
	private String lugarentrega;

	@JsonProperty("TerminosEntrega")
	private String terminosentrega;
	
	@JsonProperty("Terminos")
	private String terminos;

	@JsonProperty("ConsignarA")
	private String consignatario;

	@JsonProperty("FechaEntrega")
	private String fechaentrega;

	@JsonProperty("FechaEnvio")
	private String fechaenvio;

	@JsonProperty("PaisEmbarque")
	private String paisembarque;

	@JsonProperty("RegionEmbarque")
	private String regionembarque;

	@JsonProperty("CondicionesEnvio")
	private String condicionembarque;

	@JsonProperty("Embarcador")
	private String embarcador;

	@JsonProperty("EmbarqueParcial")
	private Integer embarqueparcial;

	@JsonProperty("PuertoDesembarque")
	private String puertodesembarque;

	@JsonProperty("Seguro")
	private String polizaseguro;

	@JsonProperty("Aduana")
	private String aduana;

	@JsonProperty("RequiereInspeccion")
	private String requiereinspeccion;

	@JsonProperty("AgenteInspeccion")
	private String companiainspectora;

	@JsonProperty("NumeroInspeccion")
	private String num_inspeccion;

	@JsonProperty("AprobadoPor")
	private String autorizadopor;

	@JsonProperty("Cargo")
	private String cargo;

	@JsonProperty("TiempoRecordatorio")
	private Integer tiemporecordatorio;

	@JsonProperty("MontoAPagar")
	private BigDecimal valortotal;

	@JsonProperty("OtrosCostos")
	private BigDecimal otroscostos;

	@JsonProperty("ValorVenta")
	private BigDecimal valorventa;

	//@JsonProperty("Descuentos")
	//private BigDecimal descuento;

	@JsonProperty("ValorVentaNeto")
	private BigDecimal subtotal;

	@JsonProperty("FechaAutorizacion")
	private String fecha_autorizacion;

	@JsonProperty("MonedaOrden")
	private String moneda;

	@JsonProperty("Comentario")
	private String comentario;

	@JsonProperty("PrecioFob")
	private BigDecimal preciofob;

	@JsonProperty("FechaEmbarque")
	private String fechaembarque;

	@JsonProperty("CodigoAlmacenEntrega")
	private String codigoalmacenenvio;

	@JsonProperty("Lab")
	private String lab;

	@JsonProperty("NumeroAp")
	private String numeroap;

	@JsonProperty("Forwader")
	private String forwader;

	@JsonProperty("ISC")
	private BigDecimal isc;

	@JsonProperty("FlagOrigen")
	private Integer flagorigen;

	@JsonProperty("Campo1")
	private String campo1;

	@JsonProperty("Campo2")
	private String campo2;

	@JsonProperty("Campo3")
	private String campo3;

	@JsonProperty("Campo4")
	private String campo4;

	@JsonProperty("Campo5")
	private String campo5;

	@JsonProperty("Campo6")
	private String campo6;

	@JsonProperty("Campo7")
	private String campo7;

	@JsonProperty("Campo8")
	private String campo8;

	@JsonProperty("CodigousUarioCreacion")
	private String codigousuariocreacion;

	@JsonProperty("CodigoUsuarioModificacion")
	private String codigousuariomodificacion;

	//@JsonProperty("FechaHoraCreacion")
	//private String fechahoracreacion;

	//@JsonProperty("FechaHoraModificacion")
	//private String fechahoramodificacion;

	@JsonProperty("IN_HABILITADO")
	private Integer habilitado;

	@JsonProperty("EstadoComprador")
	private String vc_estadocomprador;

	@JsonProperty("EstadoProveedor")
	private String vc_estadoproveedor;

	@JsonProperty("ObEstado")
	private String obsestado;

	/*@JsonProperty("FechaCreacion")
	private String fechacreacion;*/

	@JsonProperty("EnviarComprobanteA")
	private String vc_enviarfacturaa;

	@JsonProperty("TipoTransporte")
	private String tipotransporte;

	@JsonProperty("TasaCambio")
	private BigDecimal tasacambio;

	@JsonProperty("ChequePara")
	private String chequepara;

	@JsonProperty("CondicionPago")
	private String condicionpago;

	@JsonProperty("ValorCondicionesPago")
	private BigDecimal valorcondpago;

	@JsonProperty("UnidadCondicionesPago")
	private String unidcondpago;

	@JsonProperty("IdAlmacenEnvio")
	private Integer idalmacenenvio;

	@JsonProperty("CodAlmacenEnvio")
	private String codalmacenenvio;

	@JsonProperty("Narrativa")
	private String narrativa;

	@JsonProperty("Cliente")
	private String cliente;

	@JsonProperty("Tipoprecio")
	private String tipoprecio;

	@JsonProperty("OtroImpuesto")
	private Integer otrosimpuestos;

	@JsonProperty("FechaCambioOrden")
	private String fechamodoc;

	@JsonProperty("IndicadorCambioOrden")
	private String flagmodoc;

	@JsonProperty("NumeroCotizacionProveedor")
	private String numqtprov;

	@JsonProperty("MsgMail")
	private String msgmail;

	@JsonProperty("IdOperadorLog")
	private String idoperadorlog;

	@JsonProperty("EstadoB2Seller")
	private String estadob2seller;

	@JsonProperty("CompradorOrgID")
	private String idorgcompradora;

	//@JsonProperty("OrgIDVendedor")
	//private String idorgproveedora;

	@JsonProperty("Impuestos")
	private BigDecimal impuestos;

	@JsonProperty("DireccionFactura")
	private String direccionfactura;

	//@JsonProperty("PrecioQt")
	//private String precioqt;

	@JsonProperty("NumeroRfq")
	private String numerorfq;

	@JsonProperty("NombreRfq")
	private String nombrerfq;

	@JsonProperty("NumeroCotizacion")
	private String numeroqt;

	@JsonProperty("VersionCotizacion")
	private Integer versionqt;

	@JsonProperty("EmitirA")
	private String vc_facturara;

	@JsonProperty("PagaraProveedor")
	private String pagaraproveedor;

	@JsonProperty("TipoCambio")
	private BigDecimal tipocambio;

	@JsonProperty("NITVendedor")
	private String rucproveedor;

	@JsonProperty("NombreVendedor")
	private String razonsocialproveedor;

	@JsonProperty("AtencionA")
	private String atenciona;

	@JsonProperty("EmailContacto")
	private String emailcontacto;

	@JsonProperty("CodProveedorCambioEstado")
	private String cod_proveedorcambioestado;

	@JsonProperty("ComentarioComprador") // informacion complementaria
	private String comentariocomprador;

	@JsonProperty("ComentarioProveedor")
	private String comentarioproveedor;

	@JsonProperty("Descuentos")
	private BigDecimal descuentoporcentaje;

	@JsonProperty("PorcentajeImpuestos")
	private BigDecimal impuestosporcentaje;

	@JsonProperty("NITComprador")
	private String ruc_comprador;

	@JsonProperty("FechaVisualizacion")
	private String fechavisualizacion;

	@JsonProperty("Monmig")
	private String monmig;

	@JsonProperty("Logo")
	private String logo;

	@JsonProperty("Firma")
	private String firma;

	@JsonProperty("FlagOcDirecta")
	private Integer flagocdirecta;

	@JsonProperty("InspeccionRequerida")
	private Integer in_requiereinspeccion;

	@JsonProperty("NumeroAP")
	private Integer in_numeroap;

	@JsonProperty("PrecioCotizacion")
	private BigDecimal fl_precioqt;

	@JsonProperty("VersionCliente")
	private String versioncliente;

	@JsonProperty("ApNumber")
	private String apnumber;

	@JsonProperty("TipoOrden")
	private String tipooc;

	@JsonProperty("CodError")
	private String coderror;

	@JsonProperty("DescError")
	private String descerror;

	@JsonProperty("FechaDocumentoOc")
	private String fechadocumentooc;

	@JsonProperty("IdOrgPropietaria")
	private String idorgpropietaria;

	@JsonProperty("FechaNotificacion")
	private String ts_fechanotificacion;

	@JsonProperty("FechaCompromisoppto")
	private String ts_fechacompromisoppto;

	@JsonProperty("NumeroExpedientesiaf")
	private String vc_numeroexpedientesiaf;

	@JsonProperty("TipoContratacion")
	private String vc_tipocontratacion;

	@JsonProperty("ObjetoContratacion")
	private String vc_objetocontratacion;

	@JsonProperty("DescripcionContratacion")
	private String vc_descripcioncontratacion;

	@JsonProperty("UnidadesSolicitantes")
	private String vc_unidadessolicitantes;

	@JsonProperty("UnidadesInformantes")
	private String vc_unidadesinformantes;

	@JsonProperty("RazonSocialComprador")
	private String vc_razonsocialcomprador;

	@JsonProperty("CreadoPor")
	private String vc_creadopor;

	@JsonProperty("OrgIDVendedor")
	private String in_codigoproveedor;

	@JsonProperty("GrupoCompra")
	private String vc_grupocompra;

	@JsonProperty("FechaIniContrato")
	private String ts_fechainiciocontrato;

	@JsonProperty("FechaFinContrato")
	private String ts_fechafincontrato;

	@JsonProperty("Iva")
	private BigDecimal igv;

	@JsonProperty("Utilidades")
	private BigDecimal utilidades;

	@JsonProperty("GastosGen")
	private BigDecimal gastos_generales;

	@JsonProperty("Expeditor")
	private String vc_forwarder;
	
	@JsonProperty("FechaCreacion")
	private String ts_fechacreacion;
	
	@JsonProperty("CondicionesGenerales")
	private String vc_condicionesgenerales;
	
	public String getVc_condicionesgenerales() {
		return vc_condicionesgenerales;
	}

	public void setVc_condicionesgenerales(String vc_condicionesgenerales) {
		this.vc_condicionesgenerales = vc_condicionesgenerales;
	}
	

	public List<ArchivoxOC> getLista_archivoxoc() {
		return lista_archivoxoc;
	}

	public void setLista_archivoxoc(List<ArchivoxOC> lista_archivoxoc) {
		this.lista_archivoxoc = lista_archivoxoc;
	}

	public List<AtributoxOC> getLista_atributoxoc() {
		return lista_atributoxoc;
	}

	public void setLista_atributoxoc(List<AtributoxOC> lista_atributoxoc) {
		this.lista_atributoxoc = lista_atributoxoc;
	}

	public List<ProductoxOC> getLista_prodxoc() {
		return lista_prodxoc;
	}

	public void setLista_prodxoc(List<ProductoxOC> lista_prodxoc) {
		this.lista_prodxoc = lista_prodxoc;
	}

	public String getIdoc() {
		return idoc;
	}

	public void setIdoc(String idoc) {
		this.idoc = idoc;
	}

	public String getIdusuariocomprador() {
		return idusuariocomprador;
	}

	public void setIdusuariocomprador(String idusuariocomprador) {
		this.idusuariocomprador = idusuariocomprador;
	}

	public String getIdusuarioproveedor() {
		return idusuarioproveedor;
	}

	public void setIdusuarioproveedor(String idusuarioproveedor) {
		this.idusuarioproveedor = idusuarioproveedor;
	}

	public String getN_usuariocomprador() {
		return n_usuariocomprador;
	}

	public void setN_usuariocomprador(String n_usuariocomprador) {
		this.n_usuariocomprador = n_usuariocomprador;
	}

	public String getN_usuarioproveedor() {
		return n_usuarioproveedor;
	}

	public void setN_usuarioproveedor(String n_usuarioproveedor) {
		this.n_usuarioproveedor = n_usuarioproveedor;
	}

	public String getNumseguimiento() {
		return numseguimiento;
	}

	public void setNumseguimiento(String numseguimiento) {
		this.numseguimiento = numseguimiento;
	}

	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

	public Integer getTipo() {
		return tipo;
	}

	public void setTipo(Integer tipo) {
		this.tipo = tipo;
	}

	public String getPrioridad() {
		return prioridad;
	}

	public void setPrioridad(String prioridad) {
		this.prioridad = prioridad;
	}

	public String getIdqt() {
		return idqt;
	}

	public void setIdqt(String idqt) {
		this.idqt = idqt;
	}

	public String getFecharegistro() {
		return fecharegistro;
	}

	public void setFecharegistro(String fecharegistro) {
		this.fecharegistro = fecharegistro;
	}

	public String getLugarentrega() {
		return lugarentrega;
	}

	public void setLugarentrega(String lugarentrega) {
		this.lugarentrega = lugarentrega;
	}

	public String getTerminosentrega() {
		return terminosentrega;
	}

	public void setTerminosentrega(String terminosentrega) {
		this.terminosentrega = terminosentrega;
	}

	public String getTerminos() {
		return terminos;
	}

	public void setTerminos(String terminos) {
		this.terminos = terminos;
	}

	public String getConsignatario() {
		return consignatario;
	}

	public void setConsignatario(String consignatario) {
		this.consignatario = consignatario;
	}

	public String getFechaentrega() {
		return fechaentrega;
	}

	public void setFechaentrega(String fechaentrega) {
		this.fechaentrega = fechaentrega;
	}

	public String getFechaenvio() {
		return fechaenvio;
	}

	public void setFechaenvio(String fechaenvio) {
		this.fechaenvio = fechaenvio;
	}

	public String getPaisembarque() {
		return paisembarque;
	}

	public void setPaisembarque(String paisembarque) {
		this.paisembarque = paisembarque;
	}

	public String getRegionembarque() {
		return regionembarque;
	}

	public void setRegionembarque(String regionembarque) {
		this.regionembarque = regionembarque;
	}

	public String getCondicionembarque() {
		return condicionembarque;
	}

	public void setCondicionembarque(String condicionembarque) {
		this.condicionembarque = condicionembarque;
	}

	public String getEmbarcador() {
		return embarcador;
	}

	public void setEmbarcador(String embarcador) {
		this.embarcador = embarcador;
	}

	public Integer getEmbarqueparcial() {
		return embarqueparcial;
	}

	public void setEmbarqueparcial(Integer embarqueparcial) {
		this.embarqueparcial = embarqueparcial;
	}

	public String getPuertodesembarque() {
		return puertodesembarque;
	}

	public void setPuertodesembarque(String puertodesembarque) {
		this.puertodesembarque = puertodesembarque;
	}

	public String getPolizaseguro() {
		return polizaseguro;
	}

	public void setPolizaseguro(String polizaseguro) {
		this.polizaseguro = polizaseguro;
	}

	public String getAduana() {
		return aduana;
	}

	public void setAduana(String aduana) {
		this.aduana = aduana;
	}

	public String getRequiereinspeccion() {
		return requiereinspeccion;
	}

	public void setRequiereinspeccion(String requiereinspeccion) {
		this.requiereinspeccion = requiereinspeccion;
	}

	public String getCompaniainspectora() {
		return companiainspectora;
	}

	public void setCompaniainspectora(String companiainspectora) {
		this.companiainspectora = companiainspectora;
	}

	public String getNum_inspeccion() {
		return num_inspeccion;
	}

	public void setNum_inspeccion(String num_inspeccion) {
		this.num_inspeccion = num_inspeccion;
	}

	public String getAutorizadopor() {
		return autorizadopor;
	}

	public void setAutorizadopor(String autorizadopor) {
		this.autorizadopor = autorizadopor;
	}

	public String getCargo() {
		return cargo;
	}

	public void setCargo(String cargo) {
		this.cargo = cargo;
	}

	public Integer getTiemporecordatorio() {
		return tiemporecordatorio;
	}

	public void setTiemporecordatorio(Integer tiemporecordatorio) {
		this.tiemporecordatorio = tiemporecordatorio;
	}

	public BigDecimal getValortotal() {
		return valortotal;
	}

	public void setValortotal(BigDecimal valortotal) {
		this.valortotal = valortotal;
	}

	public BigDecimal getOtroscostos() {
		return otroscostos;
	}

	public void setOtroscostos(BigDecimal otroscostos) {
		this.otroscostos = otroscostos;
	}

	public BigDecimal getValorventa() {
		return valorventa;
	}

	public void setValorventa(BigDecimal valorventa) {
		this.valorventa = valorventa;
	}

	public BigDecimal getSubtotal() {
		return subtotal;
	}

	public void setSubtotal(BigDecimal subtotal) {
		this.subtotal = subtotal;
	}

	public String getFecha_autorizacion() {
		return fecha_autorizacion;
	}

	public void setFecha_autorizacion(String fecha_autorizacion) {
		this.fecha_autorizacion = fecha_autorizacion;
	}

	public String getMoneda() {
		return moneda;
	}

	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}

	public String getComentario() {
		return comentario;
	}

	public void setComentario(String comentario) {
		this.comentario = comentario;
	}

	public BigDecimal getPreciofob() {
		return preciofob;
	}

	public void setPreciofob(BigDecimal preciofob) {
		this.preciofob = preciofob;
	}

	public String getFechaembarque() {
		return fechaembarque;
	}

	public void setFechaembarque(String fechaembarque) {
		this.fechaembarque = fechaembarque;
	}

	public String getCodigoalmacenenvio() {
		return codigoalmacenenvio;
	}

	public void setCodigoalmacenenvio(String codigoalmacenenvio) {
		this.codigoalmacenenvio = codigoalmacenenvio;
	}

	public String getLab() {
		return lab;
	}

	public void setLab(String lab) {
		this.lab = lab;
	}

	public String getNumeroap() {
		return numeroap;
	}

	public void setNumeroap(String numeroap) {
		this.numeroap = numeroap;
	}

	public String getForwader() {
		return forwader;
	}

	public void setForwader(String forwader) {
		this.forwader = forwader;
	}

	public BigDecimal getIsc() {
		return isc;
	}

	public void setIsc(BigDecimal isc) {
		this.isc = isc;
	}

	public Integer getFlagorigen() {
		return flagorigen;
	}

	public void setFlagorigen(Integer flagorigen) {
		this.flagorigen = flagorigen;
	}

	public String getCampo1() {
		return campo1;
	}

	public void setCampo1(String campo1) {
		this.campo1 = campo1;
	}

	public String getCampo2() {
		return campo2;
	}

	public void setCampo2(String campo2) {
		this.campo2 = campo2;
	}

	public String getCampo3() {
		return campo3;
	}

	public void setCampo3(String campo3) {
		this.campo3 = campo3;
	}

	public String getCampo4() {
		return campo4;
	}

	public void setCampo4(String campo4) {
		this.campo4 = campo4;
	}

	public String getCampo5() {
		return campo5;
	}

	public void setCampo5(String campo5) {
		this.campo5 = campo5;
	}

	public String getCampo6() {
		return campo6;
	}

	public void setCampo6(String campo6) {
		this.campo6 = campo6;
	}

	public String getCampo7() {
		return campo7;
	}

	public void setCampo7(String campo7) {
		this.campo7 = campo7;
	}

	public String getCampo8() {
		return campo8;
	}

	public void setCampo8(String campo8) {
		this.campo8 = campo8;
	}

	public String getCodigousuariocreacion() {
		return codigousuariocreacion;
	}

	public void setCodigousuariocreacion(String codigousuariocreacion) {
		this.codigousuariocreacion = codigousuariocreacion;
	}

	public String getCodigousuariomodificacion() {
		return codigousuariomodificacion;
	}

	public void setCodigousuariomodificacion(String codigousuariomodificacion) {
		this.codigousuariomodificacion = codigousuariomodificacion;
	}

	public Integer getHabilitado() {
		return habilitado;
	}

	public void setHabilitado(Integer habilitado) {
		this.habilitado = habilitado;
	}

	public String getVc_estadocomprador() {
		return vc_estadocomprador;
	}

	public void setVc_estadocomprador(String vc_estadocomprador) {
		this.vc_estadocomprador = vc_estadocomprador;
	}

	public String getVc_estadoproveedor() {
		return vc_estadoproveedor;
	}

	public void setVc_estadoproveedor(String vc_estadoproveedor) {
		this.vc_estadoproveedor = vc_estadoproveedor;
	}

	public String getObsestado() {
		return obsestado;
	}

	public void setObsestado(String obsestado) {
		this.obsestado = obsestado;
	}

	/*public String getFechacreacion() {
		return fechacreacion;
	}

	public void setFechacreacion(String fechacreacion) {
		this.fechacreacion = fechacreacion;
	}*/


	public String getTipotransporte() {
		return tipotransporte;
	}

	public void setTipotransporte(String tipotransporte) {
		this.tipotransporte = tipotransporte;
	}

	public BigDecimal getTasacambio() {
		return tasacambio;
	}

	public void setTasacambio(BigDecimal tasacambio) {
		this.tasacambio = tasacambio;
	}

	public String getChequepara() {
		return chequepara;
	}

	public void setChequepara(String chequepara) {
		this.chequepara = chequepara;
	}

	public String getCondicionpago() {
		return condicionpago;
	}

	public void setCondicionpago(String condicionpago) {
		this.condicionpago = condicionpago;
	}

	public BigDecimal getValorcondpago() {
		return valorcondpago;
	}

	public void setValorcondpago(BigDecimal valorcondpago) {
		this.valorcondpago = valorcondpago;
	}

	public String getUnidcondpago() {
		return unidcondpago;
	}

	public void setUnidcondpago(String unidcondpago) {
		this.unidcondpago = unidcondpago;
	}

	public Integer getIdalmacenenvio() {
		return idalmacenenvio;
	}

	public void setIdalmacenenvio(Integer idalmacenenvio) {
		this.idalmacenenvio = idalmacenenvio;
	}

	public String getCodalmacenenvio() {
		return codalmacenenvio;
	}

	public void setCodalmacenenvio(String codalmacenenvio) {
		this.codalmacenenvio = codalmacenenvio;
	}

	public String getNarrativa() {
		return narrativa;
	}

	public void setNarrativa(String narrativa) {
		this.narrativa = narrativa;
	}

	public String getCliente() {
		return cliente;
	}

	public void setCliente(String cliente) {
		this.cliente = cliente;
	}

	public String getTipoprecio() {
		return tipoprecio;
	}

	public void setTipoprecio(String tipoprecio) {
		this.tipoprecio = tipoprecio;
	}

	public Integer getOtrosimpuestos() {
		return otrosimpuestos;
	}

	public void setOtrosimpuestos(Integer otrosimpuestos) {
		this.otrosimpuestos = otrosimpuestos;
	}

	public String getFechamodoc() {
		return fechamodoc;
	}

	public void setFechamodoc(String fechamodoc) {
		this.fechamodoc = fechamodoc;
	}

	public String getFlagmodoc() {
		return flagmodoc;
	}

	public void setFlagmodoc(String flagmodoc) {
		this.flagmodoc = flagmodoc;
	}

	public String getNumqtprov() {
		return numqtprov;
	}

	public void setNumqtprov(String numqtprov) {
		this.numqtprov = numqtprov;
	}

	public String getMsgmail() {
		return msgmail;
	}

	public void setMsgmail(String msgmail) {
		this.msgmail = msgmail;
	}

	public String getIdoperadorlog() {
		return idoperadorlog;
	}

	public void setIdoperadorlog(String idoperadorlog) {
		this.idoperadorlog = idoperadorlog;
	}

	public String getEstadob2seller() {
		return estadob2seller;
	}

	public void setEstadob2seller(String estadob2seller) {
		this.estadob2seller = estadob2seller;
	}

	public String getIdorgcompradora() {
		return idorgcompradora;
	}

	public void setIdorgcompradora(String idorgcompradora) {
		this.idorgcompradora = idorgcompradora;
	}

	public BigDecimal getImpuestos() {
		return impuestos;
	}

	public void setImpuestos(BigDecimal impuestos) {
		this.impuestos = impuestos;
	}

	public String getDireccionfactura() {
		return direccionfactura;
	}

	public void setDireccionfactura(String direccionfactura) {
		this.direccionfactura = direccionfactura;
	}

	public String getNumerorfq() {
		return numerorfq;
	}

	public void setNumerorfq(String numerorfq) {
		this.numerorfq = numerorfq;
	}

	public String getNombrerfq() {
		return nombrerfq;
	}

	public void setNombrerfq(String nombrerfq) {
		this.nombrerfq = nombrerfq;
	}

	public String getNumeroqt() {
		return numeroqt;
	}

	public void setNumeroqt(String numeroqt) {
		this.numeroqt = numeroqt;
	}

	public Integer getVersionqt() {
		return versionqt;
	}

	public void setVersionqt(Integer versionqt) {
		this.versionqt = versionqt;
	}


	public String getVc_enviarfacturaa() {
		return vc_enviarfacturaa;
	}

	public void setVc_enviarfacturaa(String vc_enviarfacturaa) {
		this.vc_enviarfacturaa = vc_enviarfacturaa;
	}

	public String getVc_facturara() {
		return vc_facturara;
	}

	public void setVc_facturara(String vc_facturara) {
		this.vc_facturara = vc_facturara;
	}

	public String getPagaraproveedor() {
		return pagaraproveedor;
	}

	public void setPagaraproveedor(String pagaraproveedor) {
		this.pagaraproveedor = pagaraproveedor;
	}

	public BigDecimal getTipocambio() {
		return tipocambio;
	}

	public void setTipocambio(BigDecimal tipocambio) {
		this.tipocambio = tipocambio;
	}

	public String getRucproveedor() {
		return rucproveedor;
	}

	public void setRucproveedor(String rucproveedor) {
		this.rucproveedor = rucproveedor;
	}

	public String getRazonsocialproveedor() {
		return razonsocialproveedor;
	}

	public void setRazonsocialproveedor(String razonsocialproveedor) {
		this.razonsocialproveedor = razonsocialproveedor;
	}

	public String getAtenciona() {
		return atenciona;
	}

	public void setAtenciona(String atenciona) {
		this.atenciona = atenciona;
	}

	public String getEmailcontacto() {
		return emailcontacto;
	}

	public void setEmailcontacto(String emailcontacto) {
		this.emailcontacto = emailcontacto;
	}

	public String getCod_proveedorcambioestado() {
		return cod_proveedorcambioestado;
	}

	public void setCod_proveedorcambioestado(String cod_proveedorcambioestado) {
		this.cod_proveedorcambioestado = cod_proveedorcambioestado;
	}

	public String getComentariocomprador() {
		return comentariocomprador;
	}

	public void setComentariocomprador(String comentariocomprador) {
		this.comentariocomprador = comentariocomprador;
	}

	public String getComentarioproveedor() {
		return comentarioproveedor;
	}

	public void setComentarioproveedor(String comentarioproveedor) {
		this.comentarioproveedor = comentarioproveedor;
	}

	public BigDecimal getDescuentoporcentaje() {
		return descuentoporcentaje;
	}

	public void setDescuentoporcentaje(BigDecimal descuentoporcentaje) {
		this.descuentoporcentaje = descuentoporcentaje;
	}

	public BigDecimal getImpuestosporcentaje() {
		return impuestosporcentaje;
	}

	public void setImpuestosporcentaje(BigDecimal impuestosporcentaje) {
		this.impuestosporcentaje = impuestosporcentaje;
	}

	public String getRuc_comprador() {
		return ruc_comprador;
	}

	public void setRuc_comprador(String ruc_comprador) {
		this.ruc_comprador = ruc_comprador;
	}

	public String getFechavisualizacion() {
		return fechavisualizacion;
	}

	public void setFechavisualizacion(String fechavisualizacion) {
		this.fechavisualizacion = fechavisualizacion;
	}

	public String getMonmig() {
		return monmig;
	}

	public void setMonmig(String monmig) {
		this.monmig = monmig;
	}

	public String getLogo() {
		return logo;
	}

	public void setLogo(String logo) {
		this.logo = logo;
	}

	public String getFirma() {
		return firma;
	}

	public void setFirma(String firma) {
		this.firma = firma;
	}

	public Integer getFlagocdirecta() {
		return flagocdirecta;
	}

	public void setFlagocdirecta(Integer flagocdirecta) {
		this.flagocdirecta = flagocdirecta;
	}

	public Integer getIn_requiereinspeccion() {
		return in_requiereinspeccion;
	}

	public void setIn_requiereinspeccion(Integer in_requiereinspeccion) {
		this.in_requiereinspeccion = in_requiereinspeccion;
	}

	public Integer getIn_numeroap() {
		return in_numeroap;
	}

	public void setIn_numeroap(Integer in_numeroap) {
		this.in_numeroap = in_numeroap;
	}

	public BigDecimal getFl_precioqt() {
		return fl_precioqt;
	}

	public void setFl_precioqt(BigDecimal fl_precioqt) {
		this.fl_precioqt = fl_precioqt;
	}

	public String getVersioncliente() {
		return versioncliente;
	}

	public void setVersioncliente(String versioncliente) {
		this.versioncliente = versioncliente;
	}

	public String getApnumber() {
		return apnumber;
	}

	public void setApnumber(String apnumber) {
		this.apnumber = apnumber;
	}

	public String getTipooc() {
		return tipooc;
	}

	public void setTipooc(String tipooc) {
		this.tipooc = tipooc;
	}

	public String getCoderror() {
		return coderror;
	}

	public void setCoderror(String coderror) {
		this.coderror = coderror;
	}

	public String getDescerror() {
		return descerror;
	}

	public void setDescerror(String descerror) {
		this.descerror = descerror;
	}

	public String getFechadocumentooc() {
		return fechadocumentooc;
	}

	public void setFechadocumentooc(String fechadocumentooc) {
		this.fechadocumentooc = fechadocumentooc;
	}

	public String getIdorgpropietaria() {
		return idorgpropietaria;
	}

	public void setIdorgpropietaria(String idorgpropietaria) {
		this.idorgpropietaria = idorgpropietaria;
	}

	public String getTs_fechanotificacion() {
		return ts_fechanotificacion;
	}

	public void setTs_fechanotificacion(String ts_fechanotificacion) {
		this.ts_fechanotificacion = ts_fechanotificacion;
	}

	public String getTs_fechacompromisoppto() {
		return ts_fechacompromisoppto;
	}

	public void setTs_fechacompromisoppto(String ts_fechacompromisoppto) {
		this.ts_fechacompromisoppto = ts_fechacompromisoppto;
	}

	public String getVc_numeroexpedientesiaf() {
		return vc_numeroexpedientesiaf;
	}

	public void setVc_numeroexpedientesiaf(String vc_numeroexpedientesiaf) {
		this.vc_numeroexpedientesiaf = vc_numeroexpedientesiaf;
	}

	public String getVc_tipocontratacion() {
		return vc_tipocontratacion;
	}

	public void setVc_tipocontratacion(String vc_tipocontratacion) {
		this.vc_tipocontratacion = vc_tipocontratacion;
	}

	public String getVc_objetocontratacion() {
		return vc_objetocontratacion;
	}

	public void setVc_objetocontratacion(String vc_objetocontratacion) {
		this.vc_objetocontratacion = vc_objetocontratacion;
	}

	public String getVc_descripcioncontratacion() {
		return vc_descripcioncontratacion;
	}

	public void setVc_descripcioncontratacion(String vc_descripcioncontratacion) {
		this.vc_descripcioncontratacion = vc_descripcioncontratacion;
	}

	public String getVc_unidadessolicitantes() {
		return vc_unidadessolicitantes;
	}

	public void setVc_unidadessolicitantes(String vc_unidadessolicitantes) {
		this.vc_unidadessolicitantes = vc_unidadessolicitantes;
	}

	public String getVc_unidadesinformantes() {
		return vc_unidadesinformantes;
	}

	public void setVc_unidadesinformantes(String vc_unidadesinformantes) {
		this.vc_unidadesinformantes = vc_unidadesinformantes;
	}

	public String getVc_razonsocialcomprador() {
		return vc_razonsocialcomprador;
	}

	public void setVc_razonsocialcomprador(String vc_razonsocialcomprador) {
		this.vc_razonsocialcomprador = vc_razonsocialcomprador;
	}

	public String getVc_creadopor() {
		return vc_creadopor;
	}

	public void setVc_creadopor(String vc_creadopor) {
		this.vc_creadopor = vc_creadopor;
	}

	public String getIn_codigoproveedor() {
		return in_codigoproveedor;
	}

	public void setIn_codigoproveedor(String in_codigoproveedor) {
		this.in_codigoproveedor = in_codigoproveedor;
	}

	public String getVc_grupocompra() {
		return vc_grupocompra;
	}

	public void setVc_grupocompra(String vc_grupocompra) {
		this.vc_grupocompra = vc_grupocompra;
	}

	public String getTs_fechainiciocontrato() {
		return ts_fechainiciocontrato;
	}

	public void setTs_fechainiciocontrato(String ts_fechainiciocontrato) {
		this.ts_fechainiciocontrato = ts_fechainiciocontrato;
	}

	public String getTs_fechafincontrato() {
		return ts_fechafincontrato;
	}

	public void setTs_fechafincontrato(String ts_fechafincontrato) {
		this.ts_fechafincontrato = ts_fechafincontrato;
	}

	public BigDecimal getIgv() {
		return igv;
	}

	public void setIgv(BigDecimal igv) {
		this.igv = igv;
	}

	public BigDecimal getUtilidades() {
		return utilidades;
	}

	public void setUtilidades(BigDecimal utilidades) {
		this.utilidades = utilidades;
	}

	public BigDecimal getGastos_generales() {
		return gastos_generales;
	}

	public void setGastos_generales(BigDecimal gastos_generales) {
		this.gastos_generales = gastos_generales;
	}

	public String getVc_forwarder() {
		return vc_forwarder;
	}

	public void setVc_forwarder(String vc_forwarder) {
		this.vc_forwarder = vc_forwarder;
	}

	public String getTs_fechacreacion() {
		return ts_fechacreacion;
	}

	public void setTs_fechacreacion(String ts_fechacreacion) {
		this.ts_fechacreacion = ts_fechacreacion;
	}


	
	
	
	
	
	

}
