package com.ebiz.msoclistar;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import com.ebiz.msoclistar.ProductoxOC;

public class ResponseDetOC {
	@JsonProperty("statuscode")
	private String statuscode = "0000";
	
	@JsonProperty("message")
	private String message = "Accion efectuada con exito";
	
	@JsonProperty("data")
	public Ordencompra data;
	
	@JsonProperty("data_productos")
	public List<ProductoxOC> data_productos;


	public String getStatuscode() {
		return statuscode;
	}

	public void setStatuscode(String statuscode) {
		this.statuscode = statuscode;
	}

	public String getMessage() {
		return message;
	}


	public void setMessage(String message) {
		this.message = message;
	}


	public Ordencompra getData() {
		return data;
	}


	public void setData(Ordencompra data) {
		this.data = data;
	}

	public List<ProductoxOC> getData_productos() {
		return data_productos;
	}

	public void setData_productos(List<ProductoxOC> data_productos) {
		this.data_productos = data_productos;
	}


}
