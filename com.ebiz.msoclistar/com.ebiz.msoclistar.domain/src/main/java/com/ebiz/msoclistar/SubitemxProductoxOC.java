package com.ebiz.msoclistar;

import java.math.BigDecimal;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SubitemxProductoxOC {
	
	@JsonProperty("AtributosSubItem")
	private List<AtributoxSubitemxProductoxOC> lista_atributoxsubitem;
	
	@JsonProperty("IdSubItem")
	private String in_idsubitemxprodxoc;
	
	@JsonProperty("CodigoSubItemOrg")
	private String vc_codigosubitemorg;
	
	@JsonProperty("Tipo")
	private Integer int_tiposubitem;
	
	@JsonProperty("DescripcionCorta")
	private String vc_descortasubitem;
	
	@JsonProperty("DescripcionLarga")
	private String vc_desclargasubitem;
	
	@JsonProperty("Campo1")
	private String vc_campo1;
	
	@JsonProperty("Campo2")
	private String vc_campo2;
	
	@JsonProperty("Campo3")
	private String vc_campo3;
	
	@JsonProperty("Campo4")
	private String vc_campo4;
	
	@JsonProperty("Campo5")
	private String vc_campo5;
	
	@JsonProperty("IdUsuarioCreacion")
	private String in_idusuariocreacion;
	
	@JsonProperty("IdUsuarioModificacion")
	private String in_idusuariomodificacion;
	
	@JsonProperty("FechaHoraCreacion")
	private String ts_fechahoracreacion;
	
	@JsonProperty("FechaHoraModificacion")
	private String ts_fechahoramodificacion;
	
	@JsonProperty("Habilitado")
	private Integer in_habilitado;
	
	@JsonProperty("Estado")
	private String vc_estadosubitem;
	
	@JsonProperty("IdOrganizacion")
	private String in_idorganizacion;
	
	@JsonProperty("IdPosicion")
	private Integer in_subitemposicion;
	
	@JsonProperty("FechaEntrega")
	private String ts_fechaentregasubitem;
	
	@JsonProperty("Precio")
	private BigDecimal fl_preciosubitem;
	
	@JsonProperty("Cantidad")
	private BigDecimal fl_cantidadsubitem;
	
	@JsonProperty("PrecioTotal")
	private BigDecimal fl_preciototalsubitem;
	
	@JsonProperty("IdUnidad")
	private Integer in_idunidadsubitem;
	
	@JsonProperty("CantidadPendiente")
	private BigDecimal fl_cantidadsubitempend;
	
	@JsonProperty("NumeroParte")
	private String vc_numeroparte;
	
	@JsonProperty("Posicion")
	private String vc_subitemposicion;
	
	@JsonProperty("IdRequerimiento")
	private Integer in_idrequerimiento;
	
	@JsonProperty("PosicionRequerimiento")
	private Integer in_posicionreq;
	
	@JsonProperty("UnidadSubProducto")
	private String vc_nombreunidad;

	public List<AtributoxSubitemxProductoxOC> getLista_atributoxsubitem() {
		return lista_atributoxsubitem;
	}

	public void setLista_atributoxsubitem(List<AtributoxSubitemxProductoxOC> lista_atributoxsubitem) {
		this.lista_atributoxsubitem = lista_atributoxsubitem;
	}

	public String getIn_idsubitemxprodxoc() {
		return in_idsubitemxprodxoc;
	}

	public void setIn_idsubitemxprodxoc(String in_idsubitemxprodxoc) {
		this.in_idsubitemxprodxoc = in_idsubitemxprodxoc;
	}

	public String getVc_codigosubitemorg() {
		return vc_codigosubitemorg;
	}

	public void setVc_codigosubitemorg(String vc_codigosubitemorg) {
		this.vc_codigosubitemorg = vc_codigosubitemorg;
	}

	public Integer getInt_tiposubitem() {
		return int_tiposubitem;
	}

	public void setInt_tiposubitem(Integer int_tiposubitem) {
		this.int_tiposubitem = int_tiposubitem;
	}

	public String getVc_descortasubitem() {
		return vc_descortasubitem;
	}

	public void setVc_descortasubitem(String vc_descortasubitem) {
		this.vc_descortasubitem = vc_descortasubitem;
	}

	public String getVc_desclargasubitem() {
		return vc_desclargasubitem;
	}

	public void setVc_desclargasubitem(String vc_desclargasubitem) {
		this.vc_desclargasubitem = vc_desclargasubitem;
	}

	public String getVc_campo1() {
		return vc_campo1;
	}

	public void setVc_campo1(String vc_campo1) {
		this.vc_campo1 = vc_campo1;
	}

	public String getVc_campo2() {
		return vc_campo2;
	}

	public void setVc_campo2(String vc_campo2) {
		this.vc_campo2 = vc_campo2;
	}

	public String getVc_campo3() {
		return vc_campo3;
	}

	public void setVc_campo3(String vc_campo3) {
		this.vc_campo3 = vc_campo3;
	}

	public String getVc_campo4() {
		return vc_campo4;
	}

	public void setVc_campo4(String vc_campo4) {
		this.vc_campo4 = vc_campo4;
	}

	public String getVc_campo5() {
		return vc_campo5;
	}

	public void setVc_campo5(String vc_campo5) {
		this.vc_campo5 = vc_campo5;
	}

	public String getIn_idusuariocreacion() {
		return in_idusuariocreacion;
	}

	public void setIn_idusuariocreacion(String in_idusuariocreacion) {
		this.in_idusuariocreacion = in_idusuariocreacion;
	}

	public String getIn_idusuariomodificacion() {
		return in_idusuariomodificacion;
	}

	public void setIn_idusuariomodificacion(String in_idusuariomodificacion) {
		this.in_idusuariomodificacion = in_idusuariomodificacion;
	}

	public String getTs_fechahoracreacion() {
		return ts_fechahoracreacion;
	}

	public void setTs_fechahoracreacion(String ts_fechahoracreacion) {
		this.ts_fechahoracreacion = ts_fechahoracreacion;
	}

	public String getTs_fechahoramodificacion() {
		return ts_fechahoramodificacion;
	}

	public void setTs_fechahoramodificacion(String ts_fechahoramodificacion) {
		this.ts_fechahoramodificacion = ts_fechahoramodificacion;
	}

	public Integer getIn_habilitado() {
		return in_habilitado;
	}

	public void setIn_habilitado(Integer in_habilitado) {
		this.in_habilitado = in_habilitado;
	}

	public String getVc_estadosubitem() {
		return vc_estadosubitem;
	}

	public void setVc_estadosubitem(String vc_estadosubitem) {
		this.vc_estadosubitem = vc_estadosubitem;
	}

	public String getIn_idorganizacion() {
		return in_idorganizacion;
	}

	public void setIn_idorganizacion(String in_idorganizacion) {
		this.in_idorganizacion = in_idorganizacion;
	}

	public Integer getIn_subitemposicion() {
		return in_subitemposicion;
	}

	public void setIn_subitemposicion(Integer in_subitemposicion) {
		this.in_subitemposicion = in_subitemposicion;
	}

	public String getTs_fechaentregasubitem() {
		return ts_fechaentregasubitem;
	}

	public void setTs_fechaentregasubitem(String ts_fechaentregasubitem) {
		this.ts_fechaentregasubitem = ts_fechaentregasubitem;
	}

	public BigDecimal getFl_preciosubitem() {
		return fl_preciosubitem;
	}

	public void setFl_preciosubitem(BigDecimal fl_preciosubitem) {
		this.fl_preciosubitem = fl_preciosubitem;
	}

	public BigDecimal getFl_cantidadsubitem() {
		return fl_cantidadsubitem;
	}

	public void setFl_cantidadsubitem(BigDecimal fl_cantidadsubitem) {
		this.fl_cantidadsubitem = fl_cantidadsubitem;
	}

	public String getVc_numeroparte() {
		return vc_numeroparte;
	}

	public void setVc_numeroparte(String vc_numeroparte) {
		this.vc_numeroparte = vc_numeroparte;
	}

	public String getVc_subitemposicion() {
		return vc_subitemposicion;
	}

	public void setVc_subitemposicion(String vc_subitemposicion) {
		this.vc_subitemposicion = vc_subitemposicion;
	}

	public Integer getIn_idrequerimiento() {
		return in_idrequerimiento;
	}

	public void setIn_idrequerimiento(Integer in_idrequerimiento) {
		this.in_idrequerimiento = in_idrequerimiento;
	}

	public Integer getIn_posicionreq() {
		return in_posicionreq;
	}

	public void setIn_posicionreq(Integer in_posicionreq) {
		this.in_posicionreq = in_posicionreq;
	}

	public BigDecimal getFl_preciototalsubitem() {
		return fl_preciototalsubitem;
	}

	public void setFl_preciototalsubitem(BigDecimal fl_preciototalsubitem) {
		this.fl_preciototalsubitem = fl_preciototalsubitem;
	}

	public Integer getIn_idunidadsubitem() {
		return in_idunidadsubitem;
	}

	public void setIn_idunidadsubitem(Integer in_idunidadsubitem) {
		this.in_idunidadsubitem = in_idunidadsubitem;
	}

	public BigDecimal getFl_cantidadsubitempend() {
		return fl_cantidadsubitempend;
	}

	public void setFl_cantidadsubitempend(BigDecimal fl_cantidadsubitempend) {
		this.fl_cantidadsubitempend = fl_cantidadsubitempend;
	}

	public String getVc_nombreunidad() {
		return vc_nombreunidad;
	}

	public void setVc_nombreunidad(String vc_nombreunidad) {
		this.vc_nombreunidad = vc_nombreunidad;
	}

	

}
