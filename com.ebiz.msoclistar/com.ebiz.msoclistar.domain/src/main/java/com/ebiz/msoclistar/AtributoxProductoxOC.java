package com.ebiz.msoclistar;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AtributoxProductoxOC {
	
	@JsonProperty("IdAtributoProducto")
	private String in_idatributo;
	
	@JsonProperty("ValorAtributoProducto")
	private String vc_valorenviado;
	
	@JsonProperty("UnidadAtributoProducto")
	//private Integer in_idunidad ;
	private String vc_nombreunidad;
	
	@JsonProperty("OperadorAtributoProducto")
	//private Integer in_idoperador ;
	private String vc_idoperador;

	@JsonProperty("NombreAtributoOrden")
	private String vc_nombreatributo ;
	
	@JsonProperty("CodigoUsuarioCreacion")
	private String in_codigousuariocreacion ;
	
	@JsonProperty("CodigoUsuarioModificacion")
	private String in_codigousuariomodificacion ;
	
	
	@JsonProperty("Habilitado")
	private Integer in_habilitado ;

	public String getIn_idatributo() {
		return in_idatributo;
	}

	public void setIn_idatributo(String in_idatributo) {
		this.in_idatributo = in_idatributo;
	}

	public String getVc_valorenviado() {
		return vc_valorenviado;
	}

	public void setVc_valorenviado(String vc_valorenviado) {
		this.vc_valorenviado = vc_valorenviado;
	}

	/*public Integer getIn_idunidad() {
		return in_idunidad;
	}

	public void setIn_idunidad(Integer in_idunidad) {
		this.in_idunidad = in_idunidad;
	}

	public Integer getIn_idoperador() {
		return in_idoperador;
	}

	public void setIn_idoperador(Integer in_idoperador) {
		this.in_idoperador = in_idoperador;
	}*/

	public String getVc_nombreatributo() {
		return vc_nombreatributo;
	}

	public void setVc_nombreatributo(String vc_nombreatributo) {
		this.vc_nombreatributo = vc_nombreatributo;
	}

	public String getIn_codigousuariocreacion() {
		return in_codigousuariocreacion;
	}

	public void setIn_codigousuariocreacion(String in_codigousuariocreacion) {
		this.in_codigousuariocreacion = in_codigousuariocreacion;
	}

	public Integer getIn_habilitado() {
		return in_habilitado;
	}

	public void setIn_habilitado(Integer in_habilitado) {
		this.in_habilitado = in_habilitado;
	}

	public String getVc_nombreunidad() {
		return vc_nombreunidad;
	}

	public void setVc_nombreunidad(String vc_nombreunidad) {
		this.vc_nombreunidad = vc_nombreunidad;
	}

	public String getVc_idoperador() {
		return vc_idoperador;
	}

	public void setVc_idoperador(String vc_idoperador) {
		this.vc_idoperador = vc_idoperador;
	}

	public String getIn_codigousuariomodificacion() {
		return in_codigousuariomodificacion;
	}

	public void setIn_codigousuariomodificacion(String in_codigousuariomodificacion) {
		this.in_codigousuariomodificacion = in_codigousuariomodificacion;
	}
	
	
	
//////////////////////////////////
	
	
	
}
