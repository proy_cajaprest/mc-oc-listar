package com.ebiz.msoclistar;

public class RequestDetOC {
	private String id;
	
	private char perfil;
	
	private int origen;
	
	private int cabecera;
	
	private int solo_productos;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public char getPerfil() {
		return perfil;
	}

	public void setPerfil(char perfil) {
		this.perfil = perfil;
	}

	public int getOrigen() {
		return origen;
	}

	public void setOrigen(int origen) {
		this.origen = origen;
	}

	public int getCabecera() {
		return cabecera;
	}

	public void setCabecera(int cabecera) {
		this.cabecera = cabecera;
	}

	public int getSolo_productos() {
		return solo_productos;
	}

	public void setSolo_productos(int solo_productos) {
		this.solo_productos = solo_productos;
	}
	
	
	
}
