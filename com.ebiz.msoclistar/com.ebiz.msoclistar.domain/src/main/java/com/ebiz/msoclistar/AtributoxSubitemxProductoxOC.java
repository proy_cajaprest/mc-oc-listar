package com.ebiz.msoclistar;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AtributoxSubitemxProductoxOC {
	
	@JsonProperty("IdAtributoxSubItem")
	private String in_idatributoxsubitemxprodxoc;
	
	@JsonProperty("ValorEnviado")
	private String vc_valorenviado;
	
	@JsonProperty("Mandatorio")
	private String vc_mandatorio;
	
	@JsonProperty("Modificable")
	private String vc_modificable;
	
	/*@JsonProperty("IdUnidad")
	private Integer in_idunidad;*/
	
	@JsonProperty("NombreUnidad")
	private String vc_nombreunidad;
	
	@JsonProperty("UnidadError")
	private String vc_unidaderror;
	
	/*@JsonProperty("IdOperador")
	private Integer in_idoperador;*/
	
	@JsonProperty("FlagCodificado")
	private String in_flagcodificado;
	
	@JsonProperty("ValorRealOrg")
	private String vc_valorrealorg;
	
	@JsonProperty("NombreAtributo")
	private String vc_nombreatributo;
	
	@JsonProperty("Campo1")
	private String vc_campo1;
	
	@JsonProperty("Campo2")
	private String vc_campo2;
	
	@JsonProperty("Campo3")
	private String vc_campo3;
	
	@JsonProperty("Campo4")
	private String vc_campo4;
	
	@JsonProperty("Campo5")
	private String vc_campo5;
	
	@JsonProperty("CodigoUsuarioCreacion")
	private String in_codigousuariocreacion ;
	
	@JsonProperty("CodigoUsuarioModificacion")
	private String in_codigousuariomodificacion ;
	
	@JsonProperty("FechaHoraCreacion")
	private String ts_fechahoracreacion;
	
	@JsonProperty("FechaHoraModificacion")
	private String ts_fechahoramodificacion;
	
	@JsonProperty("Habilitado")
	private Integer in_habilitado;
	
	@JsonProperty("OperadorAtributoSubProducto")
	private String vc_idoperador;

	public String getIn_idatributoxsubitemxprodxoc() {
		return in_idatributoxsubitemxprodxoc;
	}

	public void setIn_idatributoxsubitemxprodxoc(String in_idatributoxsubitemxprodxoc) {
		this.in_idatributoxsubitemxprodxoc = in_idatributoxsubitemxprodxoc;
	}

	public String getVc_valorenviado() {
		return vc_valorenviado;
	}

	public void setVc_valorenviado(String vc_valorenviado) {
		this.vc_valorenviado = vc_valorenviado;
	}

	public String getVc_mandatorio() {
		return vc_mandatorio;
	}

	public void setVc_mandatorio(String vc_mandatorio) {
		this.vc_mandatorio = vc_mandatorio;
	}

	public String getVc_modificable() {
		return vc_modificable;
	}

	public void setVc_modificable(String vc_modificable) {
		this.vc_modificable = vc_modificable;
	}

	/*public Integer getIn_idunidad() {
		return in_idunidad;
	}

	public void setIn_idunidad(Integer in_idunidad) {
		this.in_idunidad = in_idunidad;
	}*/

	public String getVc_nombreunidad() {
		return vc_nombreunidad;
	}

	public void setVc_nombreunidad(String vc_nombreunidad) {
		this.vc_nombreunidad = vc_nombreunidad;
	}

	public String getVc_unidaderror() {
		return vc_unidaderror;
	}

	public void setVc_unidaderror(String vc_unidaderror) {
		this.vc_unidaderror = vc_unidaderror;
	}

	/*public Integer getIn_idoperador() {
		return in_idoperador;
	}

	public void setIn_idoperador(Integer in_idoperador) {
		this.in_idoperador = in_idoperador;
	}*/

	public String getIn_flagcodificado() {
		return in_flagcodificado;
	}

	public void setIn_flagcodificado(String in_flagcodificado) {
		this.in_flagcodificado = in_flagcodificado;
	}

	public String getVc_valorrealorg() {
		return vc_valorrealorg;
	}

	public void setVc_valorrealorg(String vc_valorrealorg) {
		this.vc_valorrealorg = vc_valorrealorg;
	}

	public String getVc_nombreatributo() {
		return vc_nombreatributo;
	}

	public void setVc_nombreatributo(String vc_nombreatributo) {
		this.vc_nombreatributo = vc_nombreatributo;
	}

	public String getVc_campo1() {
		return vc_campo1;
	}

	public void setVc_campo1(String vc_campo1) {
		this.vc_campo1 = vc_campo1;
	}

	public String getVc_campo2() {
		return vc_campo2;
	}

	public void setVc_campo2(String vc_campo2) {
		this.vc_campo2 = vc_campo2;
	}

	public String getVc_campo3() {
		return vc_campo3;
	}

	public void setVc_campo3(String vc_campo3) {
		this.vc_campo3 = vc_campo3;
	}

	public String getVc_campo4() {
		return vc_campo4;
	}

	public void setVc_campo4(String vc_campo4) {
		this.vc_campo4 = vc_campo4;
	}

	public String getVc_campo5() {
		return vc_campo5;
	}

	public void setVc_campo5(String vc_campo5) {
		this.vc_campo5 = vc_campo5;
	}


	public String getTs_fechahoracreacion() {
		return ts_fechahoracreacion;
	}

	public void setTs_fechahoracreacion(String ts_fechahoracreacion) {
		this.ts_fechahoracreacion = ts_fechahoracreacion;
	}

	public String getTs_fechahoramodificacion() {
		return ts_fechahoramodificacion;
	}

	public void setTs_fechahoramodificacion(String ts_fechahoramodificacion) {
		this.ts_fechahoramodificacion = ts_fechahoramodificacion;
	}

	public Integer getIn_habilitado() {
		return in_habilitado;
	}

	public void setIn_habilitado(Integer in_habilitado) {
		this.in_habilitado = in_habilitado;
	}

	public String getVc_idoperador() {
		return vc_idoperador;
	}

	public void setVc_idoperador(String vc_idoperador) {
		this.vc_idoperador = vc_idoperador;
	}

	public String getIn_codigousuariocreacion() {
		return in_codigousuariocreacion;
	}

	public void setIn_codigousuariocreacion(String in_codigousuariocreacion) {
		this.in_codigousuariocreacion = in_codigousuariocreacion;
	}

	public String getIn_codigousuariomodificacion() {
		return in_codigousuariomodificacion;
	}

	public void setIn_codigousuariomodificacion(String in_codigousuariomodificacion) {
		this.in_codigousuariomodificacion = in_codigousuariomodificacion;
	}
	
	
	
}
